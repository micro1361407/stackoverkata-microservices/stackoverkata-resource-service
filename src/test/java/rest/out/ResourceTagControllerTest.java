package rest.out;

import org.junit.Test;
import org.springframework.test.context.jdbc.Sql;
import stackover.resource.service.SpringSimpleContextTest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class ResourceTagControllerTest extends SpringSimpleContextTest {

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, value = "/scripts/out/ResourceTagControllerTest/getTop10Tags_ListIsExist_ReturnListOfTopTag/BeforeTest.sql")
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, value = "/scripts/out/ResourceTagControllerTest/getTop10Tags_ListIsExist_ReturnListOfTopTag/AfterTest.sql")
    public void getTop10Tags_ListIsExist_ReturnListOfTopTag() throws Exception {
        mockMvc.perform(get("/api/user/tag/related"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].title").value("Hibernate"))
                .andExpect(jsonPath("$[9].title").value("CSS"));
    }


    @Test
    public void getTop10Tags_EmptyDatabase_ReturnEmptyListOfTopTag() throws Exception {
        mockMvc.perform(get("/api/user/tag/related"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(0));
    }
    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, value = "/scripts/out/ResourceTagControllerTest/getTop3TagsByUserId/BeforeTest.sql")
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, value = "/scripts/out/ResourceTagControllerTest/getTop3TagsByUserId/AfterTest.sql")
    public void getTop3TagsUser_ListIsExist_ReturnListOfTopTag() throws Exception {
        Long accountId = 1000L;

        mockMvc.perform(get("/api/user/tag/top-3tags")
                        .param("accountId", accountId.toString()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name").value("Hibernate"))
                .andExpect(jsonPath("$[1].name").value("SQL"))
                .andExpect(jsonPath("$[2].name").value("Postgres"));
    }

    @Test
    public void getTop3TagsUser_InvalidAccountId_ReturnBadRequest() throws Exception {
        mockMvc.perform(get("/api/user/tag/top-3tags")
                        .param("accountId", "-1"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void getTop3TagsUser_EmptyDatabase_ReturnEmptyListOfTopTag() throws Exception {
        Long accountId = 999L;

        mockMvc.perform(get("/api/user/tag/top-3tags")
                        .param("accountId", accountId.toString()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(0));
    }
}
