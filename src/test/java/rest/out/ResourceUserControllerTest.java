package rest.out;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import stackover.resource.service.SpringSimpleContextTest;
import stackover.resource.service.feign.ProfileServiceClient;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ResourceUserControllerTest extends SpringSimpleContextTest {

    @Autowired
    private ProfileServiceClient profileServiceClient;

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
            value = "/scripts/out/ResourceUserControllerTest/BeforeTest.sql")
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD,
            value = "/scripts/out/ResourceUserControllerTest/AfterTest.sql")
    public void getUserDtoByUserId_UserExists_ReturnsUser() throws Exception {
        when(profileServiceClient.getUserEmail(100L)).thenReturn("test@example.com");

        mockMvc.perform(get("/api/user/100")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(100)))
                .andExpect(jsonPath("$.email", is("test@example.com")))
                .andExpect(jsonPath("$.fullName", is("Test User")))
                .andExpect(jsonPath("$.linkImage", is("http://example.com/image.jpg")))
                .andExpect(jsonPath("$.city", is("Moscow")))
                .andExpect(jsonPath("$.reputation", is(20)))
                .andExpect(jsonPath("$.registrationDate", is("2024-01-01T10:00:00")))
                .andExpect(jsonPath("$.votes", is(4)))
                .andExpect(jsonPath("$.listTop3TagDto[0].id", is(102)))
                .andExpect(jsonPath("$.listTop3TagDto[0].name", is("Spring")))
                .andExpect(jsonPath("$.listTop3TagDto[0].description", is("description Spring")));
    }

    @Test
    public void getUserDtoByUserId_UserNotExists_ReturnsNotFound() throws Exception {
        mockMvc.perform(get("/api/user/999")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.id", is(999)))
                .andExpect(jsonPath("$.email", is("Unknown")))
                .andExpect(jsonPath("$.fullName", is("Unknown")));
    }
}

