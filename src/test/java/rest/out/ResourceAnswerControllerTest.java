package rest.out;

import jakarta.servlet.ServletException;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import stackover.resource.service.SpringSimpleContextTest;
import stackover.resource.service.dto.request.AnswerRequestDto;
import stackover.resource.service.feign.AuthServiceClient;
import java.time.LocalDateTime;

import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@Slf4j
public class ResourceAnswerControllerTest extends SpringSimpleContextTest {

    @Autowired
    private AuthServiceClient authServiceClient;

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, value = "/scripts/out/ResourceAnswerControllerTest/getAllAnswersTrueSolved/BeforeTest.sql")
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, value = "/scripts/out/ResourceAnswerControllerTest/getAllAnswersTrueSolved/AfterTest.sql")
    public void getAllAnswersTrueSolved() throws Exception {
        when(authServiceClient.isAccountExist(anyLong())).thenReturn(true);

        mockMvc.perform(get("/api/user/question/101/answer?accountId=101")).andExpect(status().isOk());
    }

    @Test
    public void getAllAnswersFalseSolved() throws Exception {
        when(authServiceClient.isAccountExist(106L)).thenReturn(false);

        mockMvc.perform(get("/api/user/question/101/answer?accountId=106")).andExpect(status().isNotFound());
    }

    @Test
    public void getAllAnswersInvalidAccountId() throws Exception {
        mockMvc.perform(get("/api/user/question/101/answer?accountId=invalid"))
                .andExpect(status().isBadRequest())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof MethodArgumentTypeMismatchException));
    }

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, value = "/scripts/out/ResourceAnswerControllerTest/getAllAnswersTrueSolved/BeforeTest.sql")
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, value = "/scripts/out/ResourceAnswerControllerTest/getAllAnswersTrueSolved/AfterTest.sql")
    public void getAllAnswersEmptyList() throws Exception {
        when(authServiceClient.isAccountExist(102L)).thenReturn(true);

        mockMvc.perform(get("/api/user/question/103/answer?accountId=102"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, value = "/scripts/out/ResourceAnswerControllerTest/getAllAnswersTrueSolved/BeforeTest.sql")
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, value = "/scripts/out/ResourceAnswerControllerTest/getAllAnswersTrueSolved/AfterTest.sql")
    public void getAllAnswersForTwoAnswers() throws Exception {
        when(authServiceClient.isAccountExist(102L)).thenReturn(true);

        mockMvc.perform(get("/api/user/question/101/answer?accountId=102"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].body").value("Ответ от Дмитрия"))
                .andExpect(jsonPath("$[1].body").value("Ответ от Дмитрия"))
                .andExpect(jsonPath("$[0].userId").value(102L))
                .andExpect(jsonPath("$[1].userId").value(102L));
    }

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, value = "/scripts/out/ResourceAnswerControllerTest/downVoteAnswerTest/BeforeTest.sql")
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, value = "/scripts/out/ResourceAnswerControllerTest/downVoteAnswerTest/AfterTest.sql")
    public void testDownVoteAnswerSuccess() throws Exception {
        when(authServiceClient.isAccountExist(102L)).thenReturn(true);
        Long totalVotes1 = entityManager.createQuery(
                "SELECT count(v) FROM VoteAnswer v WHERE v.answer.id = 100", Long.class).getSingleResult();
        Long reputation1 = entityManager.createQuery(
                "SELECT SUM(r.count) FROM Reputation r WHERE r.answer.id = 100 AND r.author.id = 101", Long.class).getSingleResult();
        log.info("До изменения голоса(положительный голос): количество голосов за ответ = {}, репутация автора = {}", totalVotes1, reputation1);
        assertTrue(totalVotes1 == 1L);
        assertTrue(reputation1 == 5L);
        mockMvc.perform(post("/api/user/question/100/answer/100/vote")
                        .param("accountId", "102").param("voteType", "DOWN"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(0));
        Long totalVotes2 = entityManager.createQuery(
                "SELECT count(v) FROM VoteAnswer v WHERE v.answer.id = 100", Long.class).getSingleResult();
        Long reputation2 = entityManager.createQuery(
                "SELECT SUM(r.count) FROM Reputation r WHERE r.answer.id = 100 AND r.author.id = 101", Long.class).getSingleResult();
        log.info("Пользователь проголосовал за понижение оценки (отмена голоса): количество голосов за ответ = {}, репутация автора = {}", totalVotes2, reputation2);
        assertTrue(totalVotes2 == 0L);
        assertTrue(reputation2 == null);
        mockMvc.perform(post("/api/user/question/100/answer/100/vote")
                        .param("accountId", "102").param("voteType", "DOWN"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(1));
        Long totalVotes3 = entityManager.createQuery(
                "SELECT count(v) FROM VoteAnswer v WHERE v.answer.id = 100", Long.class).getSingleResult();
        Long reputation3 = entityManager.createQuery(
                "SELECT SUM(r.count) FROM Reputation r WHERE r.answer.id = 100 AND r.author.id = 101", Long.class).getSingleResult();
        log.info("Пользователь снова проголосовал за понижение оценки(отрицательный голос): количество голосов за ответ = {}, репутация автора = {}", totalVotes3, reputation3);
        assertTrue(totalVotes3 == 1L);
        assertTrue(reputation3 == -5L);
    }

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, value = "/scripts/out/ResourceAnswerControllerTest/downVoteAnswerTest/BeforeTest.sql")
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, value = "/scripts/out/ResourceAnswerControllerTest/downVoteAnswerTest/AfterTest.sql")
    public void testDownVoteAnswerUserHasAlreadyVoted() throws Exception {
        when(authServiceClient.isAccountExist(103L)).thenReturn(true);
        mockMvc.perform(post("/api/user/question/100/answer/100/vote")
                        .param("accountId", "103").param("voteType", "DOWN"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(2));
        Long totalVotes1 = entityManager.createQuery(
                "SELECT count(v) FROM VoteAnswer v WHERE v.answer.id = 100", Long.class).getSingleResult();
        Long reputation1 = entityManager.createQuery(
                "SELECT SUM(r.count) FROM Reputation r WHERE r.answer.id = 100 AND r.author.id = 101", Long.class).getSingleResult();
        log.info("Пользователь проголосовал за понижение оценки: количество голосов за ответ = {}, репутация автора = {}", totalVotes1, reputation1);
        assertTrue(totalVotes1 == 2L);
        assertTrue(reputation1 == 0L);
        mockMvc.perform(post("/api/user/question/100/answer/100/vote")
                        .param("accountId", "103").param("voteType", "DOWN"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(2));
        Long totalVotes2 = entityManager.createQuery(
                "SELECT count(v) FROM VoteAnswer v WHERE v.answer.id = 100", Long.class).getSingleResult();
        Long reputation2 = entityManager.createQuery(
                "SELECT SUM(r.count) FROM Reputation r WHERE r.answer.id = 100 AND r.author.id = 101", Long.class).getSingleResult();
        log.info("Пользователь повторно проголосовал за понижение оценки: количество голосов за ответ = {}, репутация автора = {}", totalVotes2, reputation2);
        assertTrue(totalVotes2 == 2L);
        assertTrue(reputation2 == 0L);
    }

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, value = "/scripts/out/ResourceAnswerControllerTest/downVoteAnswerTest/BeforeTest.sql")
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, value = "/scripts/out/ResourceAnswerControllerTest/downVoteAnswerTest/AfterTest.sql")
    public void testDownVoteAnswerVoteForYourAnswer() throws Exception {
        when(authServiceClient.isAccountExist(101L)).thenReturn(true);
        Long totalVotes1 = entityManager.createQuery(
                "SELECT count(v) FROM VoteAnswer v WHERE v.answer.id = 100", Long.class).getSingleResult();
        Long reputation1 = entityManager.createQuery(
                "SELECT SUM(r.count) FROM Reputation r WHERE r.answer.id = 100 AND r.author.id = 101", Long.class).getSingleResult();
        log.info("Количество голосов за ответ до голосования = {}, репутация автора = {}", totalVotes1, reputation1);
        mockMvc.perform(post("/api/user/question/100/answer/100/vote")
                        .param("accountId", "101").param("voteType", "DOWN"))
                .andExpect(status().isBadRequest());
        Long totalVotes2 = entityManager.createQuery(
                "SELECT count(v) FROM VoteAnswer v WHERE v.answer.id = 100", Long.class).getSingleResult();
        Long reputation2 = entityManager.createQuery(
                "SELECT SUM(r.count) FROM Reputation r WHERE r.answer.id = 100 AND r.author.id = 101", Long.class).getSingleResult();
        log.info("Пользователь c id = {} голосует за ответ с authorId = {}", 101, 101);
        log.info("Количество голосов за ответ = {}, репутация автора = {}", totalVotes2, reputation2);
        assertTrue(totalVotes2 == 1L);
        assertTrue(reputation2 == 5L);
    }









    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, value = "/scripts/out/ResourceAnswerControllerTest/upVoteAnswerTest/BeforeTest.sql")
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, value = "/scripts/out/ResourceAnswerControllerTest/upVoteAnswerTest/AfterTest.sql")
    public void testUpVoteAnswerSuccess() throws Exception {
        when(authServiceClient.isAccountExist(102L)).thenReturn(true);
        Long totalVotes1 = entityManager.createQuery(
                "SELECT count(v) FROM VoteAnswer v WHERE v.answer.id = 100", Long.class).getSingleResult();
        Long reputation1 = entityManager.createQuery(
                "SELECT SUM(r.count) FROM Reputation r WHERE r.answer.id = 100 AND r.author.id = 101", Long.class).getSingleResult();
        log.info("До изменения голоса(положительный голос): количество голосов за ответ = {}, репутация автора = {}", totalVotes1, reputation1);
        assertTrue(totalVotes1 == 1L);
        assertTrue(reputation1 == -10L);
        mockMvc.perform(post("/api/user/question/100/answer/100/vote")
                        .param("accountId", "102").param("voteType", "UP"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(0));
        Long totalVotes2 = entityManager.createQuery(
                "SELECT count(v) FROM VoteAnswer v WHERE v.answer.id = 100", Long.class).getSingleResult();
        Long reputation2 = entityManager.createQuery(
                "SELECT SUM(r.count) FROM Reputation r WHERE r.answer.id = 100 AND r.author.id = 101", Long.class).getSingleResult();
        log.info("Пользователь проголосовал за понижение оценки (отмена голоса): количество голосов за ответ = {}, репутация автора = {}", totalVotes2, reputation2);
        assertTrue(totalVotes2 == 0L);
        assertTrue(reputation2 == null);
        mockMvc.perform(post("/api/user/question/100/answer/100/vote")
                        .param("accountId", "102").param("voteType", "UP"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(1));
        Long totalVotes3 = entityManager.createQuery(
                "SELECT count(v) FROM VoteAnswer v WHERE v.answer.id = 100", Long.class).getSingleResult();
        Long reputation3 = entityManager.createQuery(
                "SELECT SUM(r.count) FROM Reputation r WHERE r.answer.id = 100 AND r.author.id = 101", Long.class).getSingleResult();
        log.info("Пользователь снова проголосовал за понижение оценки(отрицательный голос): количество голосов за ответ = {}, репутация автора = {}", totalVotes3, reputation3);
        assertTrue(totalVotes3 == 1L);
        assertTrue(reputation3 == 10L);
    }

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, value = "/scripts/out/ResourceAnswerControllerTest/upVoteAnswerTest/BeforeTest.sql")
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, value = "/scripts/out/ResourceAnswerControllerTest/upVoteAnswerTest/AfterTest.sql")
    public void testUpVoteAnswerUserHasAlreadyVoted() throws Exception {
        when(authServiceClient.isAccountExist(103L)).thenReturn(true);
        mockMvc.perform(post("/api/user/question/100/answer/100/vote")
                        .param("accountId", "103").param("voteType", "UP"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(2));
        Long totalVotes1 = entityManager.createQuery(
                "SELECT count(v) FROM VoteAnswer v WHERE v.answer.id = 100", Long.class).getSingleResult();
        Long reputation1 = entityManager.createQuery(
                "SELECT SUM(r.count) FROM Reputation r WHERE r.answer.id = 100 AND r.author.id = 101", Long.class).getSingleResult();
        log.info("Пользователь проголосовал за повышение оценки: количество голосов за ответ = {}, репутация автора = {}", totalVotes1, reputation1);
        assertTrue(totalVotes1 == 2L);
        assertTrue(reputation1 == 0L);
        mockMvc.perform(post("/api/user/question/100/answer/100/vote")
                        .param("accountId", "103").param("voteType", "UP"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(2));
        Long totalVotes2 = entityManager.createQuery(
                "SELECT count(v) FROM VoteAnswer v WHERE v.answer.id = 100", Long.class).getSingleResult();
        Long reputation2 = entityManager.createQuery(
                "SELECT SUM(r.count) FROM Reputation r WHERE r.answer.id = 100 AND r.author.id = 101", Long.class).getSingleResult();
        log.info("Пользователь повторно проголосовал за повышение оценки: количество голосов за ответ = {}, репутация автора = {}", totalVotes2, reputation2);
        assertTrue(totalVotes2 == 2L);
        assertTrue(reputation2 == 0L);
    }

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, value = "/scripts/out/ResourceAnswerControllerTest/upVoteAnswerTest/BeforeTest.sql")
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, value = "/scripts/out/ResourceAnswerControllerTest/upVoteAnswerTest/AfterTest.sql")
    public void testUpVoteAnswerVoteForYourAnswer() throws Exception {
        when(authServiceClient.isAccountExist(101L)).thenReturn(true);
        Long totalVotes1 = entityManager.createQuery(
                "SELECT count(v) FROM VoteAnswer v WHERE v.answer.id = 100", Long.class).getSingleResult();
        Long reputation1 = entityManager.createQuery(
                "SELECT SUM(r.count) FROM Reputation r WHERE r.answer.id = 100 AND r.author.id = 101", Long.class).getSingleResult();
        log.info("Количество голосов за ответ до голосования = {}, репутация автора = {}", totalVotes1, reputation1);
        mockMvc.perform(post("/api/user/question/100/answer/100/vote")
                        .param("accountId", "101").param("voteType", "UP"))
                .andExpect(status().isBadRequest());
        Long totalVotes2 = entityManager.createQuery(
                "SELECT count(v) FROM VoteAnswer v WHERE v.answer.id = 100", Long.class).getSingleResult();
        Long reputation2 = entityManager.createQuery(
                "SELECT SUM(r.count) FROM Reputation r WHERE r.answer.id = 100 AND r.author.id = 101", Long.class).getSingleResult();
        log.info("Пользователь c id = {} голосует за ответ с authorId = {}", 101, 101);
        log.info("Количество голосов за ответ = {}, репутация автора = {}", totalVotes2, reputation2);
        assertTrue(totalVotes2 == 1L);
        assertTrue(reputation2 == -10L);
    }

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, value = "/scripts/out/ResourceAnswerControllerTest/addCommentToAnswer/BeforeTest.sql")
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, value = "/scripts/out/ResourceAnswerControllerTest/addCommentToAnswer/AfterTest.sql")
    public void addCommentToAnswer() throws Exception {
        when(authServiceClient.isAccountExist(100L)).thenReturn(true);
        String commentText = "Это тестовый комментарий";
        mockMvc.perform(post("/api/user/question/200/answer/300/comment?accountId=100")
                        .content(commentText)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.text").value(commentText))
                .andExpect(jsonPath("$.userId").value(100));
    }

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, value = "/scripts/out/ResourceAnswerControllerTest/addCommentToAnswer/BeforeTest.sql")
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, value = "/scripts/out/ResourceAnswerControllerTest/addCommentToAnswer/AfterTest.sql")
    public void addCommentToAnswerInvalidAccount() throws Exception {
        when(authServiceClient.isAccountExist(200L)).thenReturn(false);

        String commentText = "Это тестовый комментарий";
        mockMvc.perform(post("/api/user/question/101/answer/101/comment?accountId=200")
                        .content(commentText)
                        .contentType(MediaType.TEXT_PLAIN))
                .andExpect(status().isNotFound());
    }

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, value = "/scripts/out/ResourceAnswerControllerTest/addCommentToAnswer/BeforeTest.sql")
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, value = "/scripts/out/ResourceAnswerControllerTest/addCommentToAnswer/AfterTest.sql")
    public void addCommentToAnswerInvalidData() throws Exception {
        when(authServiceClient.isAccountExist(100L)).thenReturn(true);

        mockMvc.perform(post("/api/user/question/101/answer/101/comment?accountId=100")
                        .content("")
                        .contentType(MediaType.TEXT_PLAIN))
                .andExpect(status().isBadRequest());
    }


    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, value = "/scripts/out/ResourceAnswerControllerTest/updateAnswerBody/BeforeTest.sql")
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, value = "/scripts/out/ResourceAnswerControllerTest/updateAnswerBody/AfterTest.sql")
    public void updateAnswerBody_AnswerExist_ReturnAnswerResponseBeforeUpdateBody() {
        when(authServiceClient.isAccountExist(anyLong())).thenReturn(true);
        String body = entityManager.createQuery(
                "SELECT htmlBody FROM Answer WHERE id = 101L", String.class).getSingleResult();
        assertTrue(body.equals("Ответ от Дмитрия"));
    }

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, value = "/scripts/out/ResourceAnswerControllerTest/updateAnswerBody/BeforeTest.sql")
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, value = "/scripts/out/ResourceAnswerControllerTest/updateAnswerBody/AfterTest.sql")
    public void updateAnswerBody_UpdateBody_ReturnAnswerResponseAfterUpdateBody() throws Exception {
        AnswerRequestDto answerRequestDto = new AnswerRequestDto(101L, 101L, 101L, "Bulka",
                LocalDateTime.of(2023, 3, 15, 10, 30),
                LocalDateTime.of(2023, 3, 15, 10, 30));
        when(authServiceClient.isAccountExist(anyLong())).thenReturn(true);
        mockMvc.perform(put("/api/user/question/101/answer/101/body?accountId=101")
                .content(objectMapper.writeValueAsString(answerRequestDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.body").value("Bulka"));
    }

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, value = "/scripts/out/ResourceAnswerControllerTest/updateAnswerBody/BeforeTest.sql")
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, value = "/scripts/out/ResourceAnswerControllerTest/updateAnswerBody/AfterTest.sql")
    public void updateAnswerBody_UpdateEmptyBody_ThrowingException() {
        AnswerRequestDto answerRequestDto = new AnswerRequestDto(101L, 101L, 101L, null,
                LocalDateTime.of(2023, 3, 15, 10, 30),
                LocalDateTime.of(2023, 3, 15, 10, 30));
        when(authServiceClient.isAccountExist(anyLong())).thenReturn(true);
        assertThrows(ServletException.class, () ->
                mockMvc.perform(put("/api/user/question/101/answer/101/body?accountId=101")
                        .content(objectMapper.writeValueAsString(answerRequestDto))
                        .contentType(MediaType.APPLICATION_JSON)));

    }

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, value = "/scripts/out/ResourceAnswerControllerTest/updateAnswerBody_DeleteMarkTrue_ThrowingException/BeforeTest.sql")
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, value = "/scripts/out/ResourceAnswerControllerTest/updateAnswerBody_DeleteMarkTrue_ThrowingException/AfterTest.sql")
    public void updateAnswerBody_DeleteMarkTrue_ThrowingException() {
        AnswerRequestDto answerRequestDto = new AnswerRequestDto(101L, 101L, 101L, "Bulka",
                LocalDateTime.of(2023, 3, 15, 10, 30),
                LocalDateTime.of(2023, 3, 15, 10, 30));
        when(authServiceClient.isAccountExist(anyLong())).thenReturn(true);
        String message = assertThrows(ServletException.class, () ->
                mockMvc.perform(put("/api/user/question/101/answer/101/body?accountId=101")
                        .content(objectMapper.writeValueAsString(answerRequestDto))
                        .contentType(MediaType.APPLICATION_JSON))).getMessage();
        assertTrue(message.contains("Answer is deleted"));
    }

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, value = "/scripts/out/ResourceAnswerControllerTest/updateAnswerBody/BeforeTest.sql")
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, value = "/scripts/out/ResourceAnswerControllerTest/updateAnswerBody/AfterTest.sql")
    public void updateAnswerBody_InvalidIdAnswer_ThrowingException() {
        AnswerRequestDto answerRequestDto = new AnswerRequestDto(102L, 101L, 101L, "Bulka",
                LocalDateTime.of(2023, 3, 15, 10, 30),
                LocalDateTime.of(2023, 3, 15, 10, 30));
        when(authServiceClient.isAccountExist(anyLong())).thenReturn(true);
        String message = assertThrows(ServletException.class, () ->
                mockMvc.perform(put("/api/user/question/101/answer/102/body?accountId=101")
                        .content(objectMapper.writeValueAsString(answerRequestDto))
                        .contentType(MediaType.APPLICATION_JSON))).getMessage();
        assertTrue(message.contains("Answer not found"));

    }

}

