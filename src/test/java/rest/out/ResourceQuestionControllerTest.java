package rest.out;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.http.MediaType;
import stackover.resource.service.SpringSimpleContextTest;
import stackover.resource.service.feign.AuthServiceClient;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.hamcrest.Matchers.empty;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ResourceQuestionControllerTest extends SpringSimpleContextTest {

    @Autowired
    private AuthServiceClient authServiceClient;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
            value = "/scripts/out/ResourceQuestionControllerTest/getQuestionByIdTrueSolvedTest/BeforeTest.sql")
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD,
            value = "/scripts/out/ResourceQuestionControllerTest/getQuestionByIdTrueSolvedTest/AfterTest.sql")
    public void getQuestionById_ReturnsValidResponseEntity() throws Exception {
        when(authServiceClient.isAccountExist(anyLong())).thenReturn(true);

        mockMvc.perform(get("/api/user/question/100?accountId=100")
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(100)))
                .andExpect(jsonPath("$.title", is("Test Question")))
                .andExpect(jsonPath("$.authorId", is(100)))
                .andExpect(jsonPath("$.authorName", is("Test User1")))
                .andExpect(jsonPath("$.authorImage", is("http://example.com/image1.jpg")))
                .andExpect(jsonPath("$.description", is("description question")))
                .andExpect(jsonPath("$.viewCount", is(3)))
                .andExpect(jsonPath("$.authorReputation", is(10)))
                .andExpect(jsonPath("$.countAnswer", is(1)))
                .andExpect(jsonPath("$.countValuable", is(1)))
                .andExpect(jsonPath("$.persistDateTime", is("2025-01-01T10:00:00")))
                .andExpect(jsonPath("$.lastUpdateDateTime", is("2025-01-01T10:00:00")))
                .andExpect(jsonPath("$.countVote", is(3)))
                .andExpect(jsonPath("$.voteTypeQuestion", is("UP")))
                .andExpect(jsonPath("$.tags[0].id", is(100)))
                .andExpect(jsonPath("$.tags[0].name", is("Java")))
                .andExpect(jsonPath("$.tags[0].description", is("description Java")))
                .andExpect(jsonPath("$.tags[1].id", is(101)))
                .andExpect(jsonPath("$.tags[1].name", is("Spring")))
                .andExpect(jsonPath("$.tags[1].description", is("description Spring")));
    }

    @Test
    public void getQuestionById_QuestionNotFound() throws Exception {
        when(authServiceClient.isAccountExist(anyLong())).thenReturn(true);

        mockMvc.perform(get("/api/user/question/999?accountId=100")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void getQuestionById_AccountNotFound() throws Exception {
        when(authServiceClient.isAccountExist(anyLong())).thenReturn(false);

        mockMvc.perform(get("/api/user/question/100?accountId=999")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
            value = "/scripts/out/ResourceQuestionControllerTest/getQuestionByIdTrueSolvedTest/BeforeTest.sql")
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD,
            value = "/scripts/out/ResourceQuestionControllerTest/getQuestionByIdTrueSolvedTest/AfterTest.sql")
    public void getQuestionById_ReturnsEmptyTagList() throws Exception {
        when(authServiceClient.isAccountExist(anyLong())).thenReturn(true);

        jdbcTemplate.update("DELETE FROM question_has_tag WHERE question_id = 100");

        mockMvc.perform(get("/api/user/question/100?accountId=100")
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(jsonPath("$.tags", empty()));
    }

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
            value = "/scripts/out/ResourceQuestionControllerTest/getQuestionByIdTrueSolvedTest/BeforeTest.sql")
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD,
            value = "/scripts/out/ResourceQuestionControllerTest/getQuestionByIdTrueSolvedTest/AfterTest.sql")
    public void getQuestionById_ReturnsNullVoteType() throws Exception {
        when(authServiceClient.isAccountExist(anyLong())).thenReturn(true);

        jdbcTemplate.update("DELETE FROM votes_on_questions WHERE question_id = 100");

        mockMvc.perform(get("/api/user/question/100?accountId=100")
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(jsonPath("$.voteTypeQuestion").doesNotExist());
    }

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
            value = "/scripts/out/ResourceQuestionControllerTest/getQuestionByIdTrueSolvedTest/BeforeTest.sql")
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD,
            value = "/scripts/out/ResourceQuestionControllerTest/getQuestionByIdTrueSolvedTest/AfterTest.sql")
    public void getQuestionById_ZeroReputation() throws Exception {
        when(authServiceClient.isAccountExist(anyLong())).thenReturn(true);

        jdbcTemplate.update("UPDATE reputation SET count = 0 WHERE author_id = 100");

        mockMvc.perform(get("/api/user/question/100?accountId=100")
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(jsonPath("$.authorReputation", is(0)));
    }
}