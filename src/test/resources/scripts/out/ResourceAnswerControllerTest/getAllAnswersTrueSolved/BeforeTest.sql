INSERT INTO user_entity (id, dtype, account_id, full_name, persist_date, city, link_site, link_github, link_vk, about,
                         image_link, last_redaction_date, nickname)
VALUES (101, 'Type1', 101, 'Иван Иванов', NOW(), 'Москва', 'https://example.com/ivan', 'https://github.com/ivan',
        'https://vk.com/ivan', 'О Иване', 'https://example.com/images/ivan.jpg', NOW(), 'ivan'),
       (102, 'Type1', 102, 'Дмитрий Петров ', NOW(), 'Оренбург', 'https://example.com/dmitr',
        'https://github.com/dmitr', 'https://vk.com/dmitr', 'О Диме', 'https://example.com/images/dmitr.jpg', NOW(),
        'dmitr');

INSERT INTO question (id, title, description, persist_date, account_id, last_redaction_date, is_deleted)
VALUES (101, 'Вопрос 1', 'Описание для первого вопроса', NOW(), 101, NOW(), FALSE),
       (102, 'Вопрос 2', 'Описание для второго вопроса', NOW(), 102, NOW(), FALSE),
       (103, 'Вопрос 3', 'Описание для третьего вопроса', NOW(), 101, NOW(), FALSE);

INSERT INTO answer (id, persist_date, update_date, question_id, account_id, html_body, is_helpful, is_deleted,
                    is_deleted_by_moderator, date_accept_time)
VALUES (101, NOW(), NOW(), 101, 102, 'Ответ от Дмитрия', TRUE, FALSE, FALSE, NULL),
       (102, NOW(), NOW(), 102, 101, 'Ответ от Ивана', FALSE, FALSE, FALSE, NULL),
       (103, NOW(), NOW(), 101, 102, 'Ответ от Дмитрия', FALSE, FALSE, FALSE, NULL);

