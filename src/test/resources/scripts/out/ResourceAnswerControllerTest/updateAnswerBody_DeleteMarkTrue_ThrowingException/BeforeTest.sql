INSERT INTO user_entity (id, dtype, account_id, full_name, persist_date, city, link_site, link_github, link_vk, about,
                         image_link, last_redaction_date, nickname)
VALUES (101, 'Type1', 101, 'Иван Иванов', '2024-01-01 11:00:00', 'Москва', 'https://example.com/ivan', 'https://github.com/ivan',
        'https://vk.com/ivan', 'О Иване', 'https://example.com/images/ivan.jpg', '2024-01-01 11:00:00', 'ivan');


INSERT INTO question (id, title, description, persist_date, account_id, last_redaction_date, is_deleted)
VALUES (101, 'Вопрос 1', 'Описание для первого вопроса', '2024-01-01 11:00:00', 101, '2024-01-01 11:00:00', FALSE);


INSERT INTO answer (id, persist_date, update_date, question_id, account_id, html_body, is_helpful, is_deleted,
                    is_deleted_by_moderator, date_accept_time)
VALUES (101, '2024-01-01 11:00:00', '2024-01-01 11:00:00', 101, 101, 'Ответ от Дмитрия', TRUE, TRUE, FALSE, NULL);