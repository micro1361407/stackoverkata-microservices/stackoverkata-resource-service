INSERT INTO user_entity (id, dtype, account_id, last_redaction_date)
VALUES (101, 'association',101, NOW()),
       (102, 'association',102, NOW()),
       (100, 'association',100, NOW()),
       (103, 'association',103, NOW());

INSERT INTO question (id, title, description, account_id, last_redaction_date)
VALUES (100, 'Вопрос', 'Описание для вопроса', 100, NOW());

INSERT INTO answer (id, update_date, question_id, account_id, html_body, is_helpful, is_deleted,
                    is_deleted_by_moderator)
VALUES (100, NOW(), 100, 101, 'Ответ', TRUE, FALSE, FALSE);

INSERT INTO votes_on_answers(id, answer_id, user_id, vote_type_answer)
VALUES (100, 100, 102, 'UP');

INSERT INTO reputation(id, answer_id, author_id, sender_id, type, count)
VALUES (100, 100,101, 102, 2, 5);




