INSERT INTO user_entity (dtype, id, account_id, full_name, persist_date, city, link_site, link_github, link_vk, about, image_link, last_redaction_date, nickname)
VALUES ('association', 100, 1000, 'Test User', '2024-01-01 10:00:00', 'Moscow', 'http://test.com', 'http://github.com/test', 'http://vk.com/test', 'Test', 'http://example.com/image.jpg', '2023-01-01 10:00:00', 'test');

INSERT INTO user_entity (dtype, id, account_id, full_name, persist_date, city, link_site, link_github, link_vk, about, image_link, last_redaction_date, nickname)
VALUES ('User', 101, 1001, 'Test User1', '2024-01-01 10:00:00', 'Moscow', 'http://test1.com', 'http://github.com/test1', 'http://vk.com/test1', 'Test1', 'http://example.com/image1.jpg', '2023-01-01 10:00:00', 'test1');


INSERT INTO question (is_deleted, account_id, id, last_redaction_date, persist_date, description, title)
VALUES ('false', 100, 200, '2024-01-01 11:00:00', '2024-01-01 11:00:00', 'java', 'java');

INSERT INTO answer (is_deleted, is_deleted_by_moderator, is_helpful, account_id, date_accept_time, id, question_id, update_date, html_body, persist_date)
VALUES ('false', 'false', 'true', 100, '2024-01-01 11:00:00', 300, 200, '2024-01-01 11:00:00', 'java1', '2024-01-01 11:00:00');

INSERT INTO answer (is_deleted, is_deleted_by_moderator, is_helpful, account_id, date_accept_time, id, question_id, update_date, html_body, persist_date)
VALUES ('false', 'false', 'true', 100, '2024-01-01 11:00:00', 301, 200, '2024-01-01 11:00:00', 'java1', '2024-01-01 11:00:00');

INSERT INTO comment (id, text, comment_type, persist_date, last_redaction_date, user_id)
VALUES (101, 'Это комментарий к ответу от Дмитрия', 1, NOW(), NOW(), 100),
       (102, 'Это комментарий к ответу от Ивана', 1, NOW(), NOW(), 101);

INSERT INTO comment_answer (comment_text_id, answer_id)
VALUES  (102, 301);
