INSERT INTO tag (id, name, description, persist_date)
VALUES (101, 'Hibernate', 'about Hibernate', '2024-01-01 11:00:00'),
       (102, 'SQL', 'about SQL', '2024-01-01 11:00:00'),
       (103, 'Postgres', 'about Postgres', '2024-01-01 11:00:00');


INSERT INTO user_entity (dtype, id, account_id, full_name, persist_date, city, link_site, link_github, link_vk, about, image_link, last_redaction_date, nickname)
VALUES ('association', 100, 1000, 'Test User', '2024-01-01 10:00:00', 'Moscow', 'http://test.com', 'http://github.com/test', 'http://vk.com/test', 'Test', 'http://example.com/image.jpg', '2023-01-01 10:00:00', 'test');

INSERT INTO user_entity (dtype, id, account_id, full_name, persist_date, city, link_site, link_github, link_vk, about, image_link, last_redaction_date, nickname)
VALUES ('User', 101, 1001, 'Test User1', '2024-01-01 10:00:00', 'Moscow', 'http://test1.com', 'http://github.com/test1', 'http://vk.com/test1', 'Test1', 'http://example.com/image1.jpg', '2023-01-01 10:00:00', 'test1');


INSERT INTO question (id, title, description, persist_date, account_id, last_redaction_date, is_deleted)
VALUES (101, 'Вопрос 1', 'Описание для первого вопроса', '2024-01-01 11:00:00', 100, '2024-01-01 11:00:00', FALSE),
       (102, 'Вопрос 2', 'Описание для второго вопроса', '2024-01-01 11:00:00', 100, '2024-01-01 11:00:00', FALSE),
       (103, 'Вопрос 3', 'Описание для третьего вопроса', '2024-01-01 11:00:00', 100, '2024-01-01 11:00:00', FALSE);

INSERT INTO question_has_tag (question_id, tag_id)
VALUES (101, 101),
       (102, 102),
       (103, 103);

INSERT INTO reputation (id, persist_date, author_id, sender_id, count, type, question_id, answer_id)
VALUES (100, '2024-01-04 16:00:00', 100, 100, 10, 2, 101, NULL);

INSERT INTO reputation (id, persist_date, author_id, sender_id, count, type, question_id, answer_id)
VALUES (101, '2024-01-04 16:00:00', 100, 100, 10, 2, 102, NULL);
INSERT INTO reputation (id, persist_date, author_id, sender_id, count, type, question_id, answer_id)
VALUES (102, '2024-01-04 16:00:00', 100, 100, 10, 2, 103, NULL);