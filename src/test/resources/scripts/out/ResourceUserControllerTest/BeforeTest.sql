INSERT INTO user_entity (dtype, id, account_id, full_name, persist_date, city, link_site, link_github, link_vk, about, image_link, last_redaction_date, nickname)
VALUES ('association', 100, 1000, 'Test User', '2024-01-01 10:00:00', 'Moscow', 'http://test.com', 'http://github.com/test', 'http://vk.com/test', 'Test', 'http://example.com/image.jpg', '2023-01-01 10:00:00', 'test');

INSERT INTO user_entity (dtype, id, account_id, full_name, persist_date, city, link_site, link_github, link_vk, about, image_link, last_redaction_date, nickname)
VALUES ('User', 101, 1001, 'Test User1', '2024-01-01 10:00:00', 'Moscow', 'http://test1.com', 'http://github.com/test1', 'http://vk.com/test1', 'Test1', 'http://example.com/image1.jpg', '2023-01-01 10:00:00', 'test1');

INSERT INTO user_entity (dtype, id, account_id, full_name, persist_date, city, link_site, link_github, link_vk, about, image_link, last_redaction_date, nickname)
VALUES ('association', 102, 100, 'Test User', '2024-01-01 10:00:00', 'Moscow', 'http://test.com', 'http://github.com/test', 'http://vk.com/test', 'Test', 'http://example.com/image.jpg', '2023-01-01 10:00:00', 'test');

INSERT INTO question (is_deleted, account_id, id, last_redaction_date, persist_date, description, title)
VALUES ('false', 100, 200, '2024-01-01 11:00:00', '2024-01-01 11:00:00', 'java', 'java');


INSERT INTO question (is_deleted, account_id, id, last_redaction_date, persist_date, description, title)
VALUES ('false', 102, 201, '2024-01-01 11:00:00', '2024-01-01 11:00:00', 'java', 'java');


INSERT INTO tag (id, name, description, persist_date)
VALUES (100, 'Java', 'description Java', '2025-01-01 10:00:00'),
       (101, 'Spring', 'description Spring', '2025-01-01 10:00:00'),
       (102, 'Spring', 'description Spring', '2025-01-01 10:00:00');

INSERT INTO question_has_tag (question_id, tag_id)
VALUES (200, 100),
       (200, 101),
       (201, 102);

INSERT INTO answer (is_deleted, is_deleted_by_moderator, is_helpful, account_id, date_accept_time, id, question_id, update_date, html_body, persist_date)
VALUES ('false', 'false', 'true', 100, '2024-01-01 11:00:00', 300, 200, '2024-01-01 11:00:00', 'java1', '2024-01-01 11:00:00');

INSERT INTO votes_on_questions (id, user_id, question_id, persist_date, vote_type_question)
VALUES (100, 100, 200, '2024-01-02 10:00:00', 'UP');

INSERT INTO votes_on_answers (id, user_id, answer_id, persist_date, vote_type_answer)
VALUES (100, 100, 300, '2024-01-03 10:00:00', 'DOWN');

INSERT INTO reputation (id, persist_date, author_id, sender_id, count, type, question_id, answer_id)
VALUES (100, '2024-01-04 16:00:00', 100, 101, 10, 1, 200, NULL);

INSERT INTO reputation (id, persist_date, author_id, sender_id, count, type, question_id, answer_id)
VALUES (101, '2024-01-04 16:00:00', 100, 101, 10, 2, 201, NULL);