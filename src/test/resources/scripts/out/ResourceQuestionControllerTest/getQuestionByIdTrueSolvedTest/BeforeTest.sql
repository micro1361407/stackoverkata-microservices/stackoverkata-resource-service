INSERT INTO user_entity (dtype, id, account_id, full_name, persist_date, city, link_site, link_github, link_vk, about, image_link, last_redaction_date, nickname)
VALUES ('type1', 100, 100, 'Test User1', '2025-01-01 10:00:00', 'Moscow', 'http://test1.com', 'http://github.com/test1', 'http://vk.com/test1', 'Test1', 'http://example.com/image1.jpg', '2025-01-01 10:00:00', 'test1'),
       ('type2', 101, 101, 'Test User2', '2025-01-01 10:00:00', 'Moscow', 'http://test2.com', 'http://github.com/test2', 'http://vk.com/test2', 'Test2', 'http://example.com/image2.jpg', '2025-01-01 10:00:00', 'test2'),
       ('type3', 102, 102, 'Test User3', '2025-01-01 10:00:00', 'Moscow', 'http://test3.com', 'http://github.com/test3', 'http://vk.com/test3', 'Test3', 'http://example.com/image3.jpg', '2025-01-01 10:00:00', 'test3');

INSERT INTO question (id, title, description, persist_date, account_id, last_redaction_date, is_deleted)
VALUES (100, 'Test Question', 'description question', '2025-01-01 10:00:00', 100, '2025-01-01 10:00:00', false);

INSERT INTO tag (id, name, description, persist_date)
VALUES (100, 'Java', 'description Java', '2025-01-01 10:00:00'),
       (101, 'Spring', 'description Spring', '2025-01-01 10:00:00');

INSERT INTO question_has_tag (question_id, tag_id)
VALUES (100, 100),
       (100, 101);

INSERT INTO question_viewed (id, account_id, question_id, persist_date)
VALUES (100, 100, 100, '2025-01-01 10:00:00'),
       (101, 100, 100, '2025-01-01 10:05:00'),
       (102, 100, 100, '2025-01-01 10:10:00');

INSERT INTO votes_on_questions (id, user_id, question_id, persist_date, vote_type_question)
VALUES (100, 100, 100, '2025-01-01 10:00:00', 'UP'),
       (101, 101, 100, '2025-01-01 11:00:00', 'UP'),
       (102, 102, 100, '2025-01-01 12:00:00', 'DOWN');

INSERT INTO answer (id, persist_date, update_date, question_id, account_id, html_body, is_helpful, is_deleted, is_deleted_by_moderator, date_accept_time)
VALUES (100, '2025-01-01 10:00:00', '2025-01-01 10:00:00', 100, 100, 'test answer', false, false, false, null);

INSERT INTO reputation (id, persist_date, author_id, sender_id, count, type, question_id, answer_id)
VALUES (100, '2025-01-01 10:00:00', 100, 100, 10, 1, 100, null);
