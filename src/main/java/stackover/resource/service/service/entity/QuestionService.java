package stackover.resource.service.service.entity;

import stackover.resource.service.dto.request.QuestionCreateRequestDto;


public interface QuestionService {

    void addNewQuestion(QuestionCreateRequestDto requestDto, Long accountId);
}