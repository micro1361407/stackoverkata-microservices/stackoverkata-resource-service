package stackover.resource.service.service.entity.impl;

import org.springframework.stereotype.Service;
import stackover.resource.service.entity.user.User;
import stackover.resource.service.repository.entity.UserRepository;
import stackover.resource.service.service.entity.UserService;

@Service("userEntityService")
public class UserServiceImpl extends AbstractServiceImpl<User, Long> implements UserService {

    public UserServiceImpl(UserRepository userRepository) {
        super(userRepository);
    }
}
