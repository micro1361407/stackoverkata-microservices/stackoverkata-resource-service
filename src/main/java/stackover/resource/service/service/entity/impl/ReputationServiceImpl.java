package stackover.resource.service.service.entity.impl;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import stackover.resource.service.entity.question.Question;
import stackover.resource.service.entity.question.answer.Answer;
import stackover.resource.service.entity.user.User;
import stackover.resource.service.entity.user.reputation.Reputation;
import stackover.resource.service.entity.user.reputation.ReputationType;
import stackover.resource.service.repository.entity.ReputationRepository;
import stackover.resource.service.service.entity.AnswerService;
import stackover.resource.service.service.entity.ReputationService;
import stackover.resource.service.service.entity.UserService;

import java.util.Optional;

@Service
public class ReputationServiceImpl extends AbstractServiceImpl<Reputation, Long> implements ReputationService {

    private final ReputationRepository reputationRepository;
    private final UserService userService;
    private final AnswerService answerService;

    public ReputationServiceImpl(ReputationRepository reputationRepository, UserService userService, AnswerService answerService) {
        super(reputationRepository);
        this.reputationRepository = reputationRepository;
        this.userService = userService;
        this.answerService = answerService;
    }

    public void updateReputation(Long senderId, Long answerOrQuestionId, ReputationType reputationType, Integer count) {
        if (reputationType == ReputationType.ANSWER) {
            Answer answer = answerService.findById(answerOrQuestionId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Ответ не найден"));
            User sender = userService.findById(senderId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Пользователь не найден"));
            Long authorId = answer.getUser().getId();
            Optional<Reputation> reputationOptional = reputationRepository.findByAuthorIdAndSenderIdAndAnswerId(authorId, senderId, answerOrQuestionId);
            if (reputationOptional.isPresent()) {
                Reputation oldReputation = reputationOptional.get();
                reputationRepository.deleteById(oldReputation.getId());
                if (oldReputation.getCount().equals(count)) {
                    createReputation(answer, null, sender, reputationType, count);
                }
            } else {
                createReputation(answer, null, sender, reputationType, count);
            }
        }
    }

    private void createReputation(Answer answer, Question question, User sender, ReputationType reputationType, Integer count) {
        Reputation reputation = new Reputation();
        reputation.setAnswer(answer);
        reputation.setQuestion(question);
        reputation.setSender(sender);
        reputation.setAuthor(answer.getUser());
        reputation.setType(reputationType);
        reputation.setCount(count);
        reputationRepository.save(reputation);
    }
}
