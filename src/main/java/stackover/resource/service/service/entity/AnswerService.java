package stackover.resource.service.service.entity;

import stackover.resource.service.dto.request.AnswerRequestDto;
import stackover.resource.service.entity.question.answer.Answer;


public interface AnswerService extends AbstractService<Answer, Long> {
    void updateBodyAnswer(Long answerId, AnswerRequestDto answerDto);
}
