package stackover.resource.service.service.entity.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import stackover.resource.service.converters.TagMapper;
import stackover.resource.service.dto.responce.TagResponseDto;
import stackover.resource.service.entity.question.Tag;
import stackover.resource.service.repository.entity.TagRepository;
import stackover.resource.service.service.entity.TagService;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class TagServiceImpl implements TagService {

    private final TagRepository tagRepository;
    private final TagMapper tagMapper;

    public TagServiceImpl(TagRepository tagRepository, TagMapper tagMapper) {
        this.tagRepository = tagRepository;
        this.tagMapper = tagMapper;
    }

    @Override
    @Transactional
    public List<Tag> saveAll(List<TagResponseDto> tags) {
        return tagRepository.saveAll(checkTagsExist(tags));
    }

    private List<Tag> checkTagsExist(List<TagResponseDto> tags) {

        List<String> tagNames = tags
                .stream()
                .map(TagResponseDto::name)
                .toList();
        List<Tag> existingTags = tagRepository.findAllByNameIn(tagNames);
        Set<String> existingTagNames = existingTags
                .stream()
                .map(Tag::getName).collect(Collectors.toSet());

        List<Tag> newTags = tags.stream()
                .filter(tagResponseDto -> !existingTagNames.contains(tagResponseDto.name()))
                .map(tagMapper::DtoToEntity)
                .toList();
        List<Tag> allTags = new ArrayList<>(existingTags);
        allTags.addAll(newTags);

        return allTags;
    }
    @Override
    public  List<Tag> findTagsByQuestionId(Long questionId) {
        return tagRepository.findTagsByQuestionId(questionId);
    }

}

