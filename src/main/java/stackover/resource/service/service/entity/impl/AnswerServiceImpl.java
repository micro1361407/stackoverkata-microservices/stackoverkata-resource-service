package stackover.resource.service.service.entity.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import stackover.resource.service.dto.request.AnswerRequestDto;
import stackover.resource.service.entity.question.answer.Answer;
import stackover.resource.service.exception.AnswerException;
import stackover.resource.service.repository.entity.AnswerRepository;
import stackover.resource.service.service.entity.AnswerService;

import java.time.LocalDateTime;

@Service
public class AnswerServiceImpl extends AbstractServiceImpl<Answer, Long> implements AnswerService {
    private final AnswerRepository answerRepository;

    public AnswerServiceImpl(AnswerRepository answerRepository) {
        super(answerRepository);
        this.answerRepository = answerRepository;
    }

    @Override
    @Transactional
    public void updateBodyAnswer(Long answerId, AnswerRequestDto answerDto) {
        Answer answer = answerRepository.findById(answerId).orElseThrow(() -> new AnswerException("Answer not found"));
        if (answer.getIsDeleted()) {
            throw new AnswerException("Answer is deleted");
        }
        if (answerId.equals(answerDto.id())) {
            answer.setHtmlBody(answerDto.body());
            answer.setUpdateDateTime(LocalDateTime.now());
            answerRepository.saveAndFlush(answer);
        }
    }
}
