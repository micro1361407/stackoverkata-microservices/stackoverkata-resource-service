package stackover.resource.service.service.entity;

import stackover.resource.service.entity.user.reputation.Reputation;
import stackover.resource.service.entity.user.reputation.ReputationType;



public interface ReputationService extends AbstractService<Reputation, Long> {
    void updateReputation(Long senderId, Long authorId, ReputationType reputationType, Integer count);
}
