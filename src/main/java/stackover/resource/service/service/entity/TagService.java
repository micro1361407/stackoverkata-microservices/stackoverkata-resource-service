package stackover.resource.service.service.entity;

import stackover.resource.service.dto.responce.TagResponseDto;
import stackover.resource.service.entity.question.Tag;

import java.util.List;

public interface TagService {

    List<Tag> saveAll(List<TagResponseDto> tags);

    List<Tag> findTagsByQuestionId(Long questionId);
}
