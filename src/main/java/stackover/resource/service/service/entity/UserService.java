package stackover.resource.service.service.entity;

import stackover.resource.service.entity.user.User;


public interface UserService extends AbstractService<User, Long> {
}
