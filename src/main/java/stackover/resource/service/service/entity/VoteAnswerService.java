package stackover.resource.service.service.entity;

import stackover.resource.service.entity.question.answer.VoteAnswer;


public interface VoteAnswerService extends AbstractService<VoteAnswer, Long> {
    long countByAnswerId(Long answerId);
    void createNewDownVoteAnswer(Long votingUserId, Long answerId);
    void createNewUpVoteAnswer(Long votingUserId, Long answerId);
}
