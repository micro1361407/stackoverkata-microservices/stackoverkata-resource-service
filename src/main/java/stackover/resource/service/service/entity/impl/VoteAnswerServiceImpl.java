package stackover.resource.service.service.entity.impl;


import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import stackover.resource.service.entity.question.answer.Answer;
import stackover.resource.service.entity.question.answer.VoteAnswer;
import stackover.resource.service.entity.question.answer.VoteTypeAnswer;
import stackover.resource.service.entity.user.User;
import stackover.resource.service.repository.entity.VoteAnswerRepository;
import stackover.resource.service.service.entity.AnswerService;
import stackover.resource.service.service.entity.UserService;
import stackover.resource.service.service.entity.VoteAnswerService;

import java.util.Optional;

@Service
public class VoteAnswerServiceImpl extends AbstractServiceImpl<VoteAnswer, Long> implements VoteAnswerService {

    private final VoteAnswerRepository voteAnswerRepository;
    private final UserService userService;
    private final AnswerService answerService;


    public VoteAnswerServiceImpl(VoteAnswerRepository voteAnswerRepository, UserService userService, AnswerService answerService) {
        super(voteAnswerRepository);
        this.voteAnswerRepository = voteAnswerRepository;
        this.userService = userService;
        this.answerService = answerService;
    }

    @Override
    public long countByAnswerId(Long answerId) {
        return voteAnswerRepository.countVoteAnswersByAnswerId(answerId);
    }

    @Override
    public void createNewDownVoteAnswer(Long votingUserId, Long answerId) {
        Answer answer = answerService.findById(answerId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Ответ не найден"));
        User votingUser = userService.findById(votingUserId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Пользователь не найден"));
        Long authorId = answer.getUser().getId();
        if (authorId.equals(votingUserId)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Нельзя голосовать за свой ответ");
        }
        Optional<VoteAnswer> existingVoteOptional = voteAnswerRepository.findByAnswerIdAndUserId(answerId, votingUserId);
        if (existingVoteOptional.isPresent()) {
            VoteAnswer existingVote = existingVoteOptional.get();
            if (existingVote.getVoteTypeAnswer() == VoteTypeAnswer.DOWN) {
                voteAnswerRepository.delete(existingVote);
                voteAnswerRepository.save(new VoteAnswer(votingUser, answer, VoteTypeAnswer.DOWN));
            } else {
                voteAnswerRepository.delete(existingVote);
            }
        } else {
            voteAnswerRepository.save(new VoteAnswer(votingUser, answer, VoteTypeAnswer.DOWN));
        }
    }

    @Override
    public void createNewUpVoteAnswer(Long votingUserId, Long answerId) {
        Answer answer = answerService.findById(answerId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Ответ не найден"));
        User votingUser = userService.findById(votingUserId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Пользователь не найден"));
        Long authorId = answer.getUser().getId();
        if (authorId.equals(votingUserId)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Нельзя голосовать за свой ответ");
        }
        Optional<VoteAnswer> existingVoteOptional = voteAnswerRepository.findByAnswerIdAndUserId(answerId, votingUserId);
        if (existingVoteOptional.isPresent()) {
            VoteAnswer existingVote = existingVoteOptional.get();
            if (existingVote.getVoteTypeAnswer() == VoteTypeAnswer.UP) {
                voteAnswerRepository.delete(existingVote);
                voteAnswerRepository.save(new VoteAnswer(votingUser, answer, VoteTypeAnswer.UP));
            } else {
                voteAnswerRepository.delete(existingVote);
            }
        } else {
            voteAnswerRepository.save(new VoteAnswer(votingUser, answer, VoteTypeAnswer.UP));
        }
    }
    }

//    @Override
//    public void createNewUpVoteAnswer(Long votingUserId, Long answerId) {
//
//        Long authorId = answer.getUser().getId();
//        if (authorId.equals(votingUserId)) {
//            throw new RuntimeException("Нельзя голосовать за свой ответ.");
//        }
//        Optional<VoteAnswer> existingVoteOptional = voteAnswerRepository.findByAnswerIdAndUserId(answerId, votingUserId);
//        if (existingVoteOptional.isPresent()) {
//            VoteAnswer existingVote = existingVoteOptional.get();
//            if (existingVote.getVoteTypeAnswer() == VoteTypeAnswer.UP) {
//                throw new RuntimeException("Пользователь уже голосовал за увеличение репутации");
//            } else {
//                voteAnswerRepository.delete(existingVote);
//                Optional<Reputation> reputationOptional =
//                        reputationService.findByAuthorIdAndSenderIdAndAnswerId(authorId, votingUserId, answerId);
//                if (reputationOptional.isPresent()) {
//                    Reputation reputation = reputationOptional.get();
//                    reputationService.deleteById(reputation.getId());
//                }
//            }
//        } else {
//            VoteAnswer voteAnswer = new VoteAnswer(votingUser, answer, VoteTypeAnswer.UP);
//            voteAnswerRepository.save(voteAnswer);
//            Optional<Reputation> reputationOptional =
//                    reputationService.findByAuthorIdAndSenderIdAndAnswerId(authorId, votingUserId, answerId);
//            if (reputationOptional.isPresent()) {
//                Reputation reputation = reputationOptional.get();
//                reputation.setCount(+10);
//                reputationRepository.save(reputation);
//            } else {
//                Reputation reputation = new Reputation();
//                reputation.setAuthor(answer.getUser());
//                reputation.setSender(votingUser);
//                reputation.setCount(+10);
//                reputation.setType(ReputationType.VOTE_ANSWER);
//                reputation.setAnswer(answer);
//                reputationService.save(reputation);
//            }
//        }
//    }

