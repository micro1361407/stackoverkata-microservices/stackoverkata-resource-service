package stackover.resource.service.service.facade.impl;

import org.springframework.stereotype.Service;
import stackover.resource.service.entity.user.reputation.ReputationType;
import stackover.resource.service.service.entity.ReputationService;
import stackover.resource.service.service.entity.VoteAnswerService;
import stackover.resource.service.service.facade.VoteAnswerFacade;



@Service
public class VoteAnswerFacadeImpl implements VoteAnswerFacade {
    private final VoteAnswerService voteAnswerService;
    private final ReputationService reputationService;

    public VoteAnswerFacadeImpl(VoteAnswerService voteAnswerService, ReputationService reputationService) {
        this.voteAnswerService = voteAnswerService;
        this.reputationService = reputationService;

    }

    @Override
    public Long createNewVoteAnswer(Long senderId, Long answerId, String voteTypeAnswer) {
        if (voteTypeAnswer.contains("UP")) {
            voteAnswerService.createNewUpVoteAnswer(senderId, answerId);
            reputationService.updateReputation(senderId, answerId, ReputationType.ANSWER, 10);
        } else if (voteTypeAnswer.contains("DOWN")) {
            voteAnswerService.createNewDownVoteAnswer(senderId, answerId);
            reputationService.updateReputation(senderId, answerId, ReputationType.ANSWER, -5);
        } else throw new IllegalArgumentException("Некорректный тип голоса: " + voteTypeAnswer);
        return voteAnswerService.countByAnswerId(answerId);
    }
}
