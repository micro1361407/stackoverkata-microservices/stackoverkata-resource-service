package stackover.resource.service.service.facade;

public interface VoteAnswerFacade {
    Long createNewVoteAnswer(Long votingUserId, Long answerId, String voteTypeAnswer);
}
