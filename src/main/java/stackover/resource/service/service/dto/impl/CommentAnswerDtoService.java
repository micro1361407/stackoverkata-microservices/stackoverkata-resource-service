package stackover.resource.service.service.dto.impl;


import jakarta.validation.constraints.Positive;
import stackover.resource.service.dto.responce.CommentAnswerResponseDto;
import stackover.resource.service.entity.question.answer.CommentAnswer;

import java.util.Optional;

public interface CommentAnswerDtoService {
    Optional<CommentAnswerResponseDto> addCommentToAnswer(CommentAnswer commentAnswer, @Positive Long accountId);

}