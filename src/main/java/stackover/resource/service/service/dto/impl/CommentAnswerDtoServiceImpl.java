package stackover.resource.service.service.dto.impl;

import jakarta.validation.constraints.Positive;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import stackover.resource.service.dto.responce.CommentAnswerResponseDto;
import stackover.resource.service.entity.question.answer.CommentAnswer;
import stackover.resource.service.feign.ProfileServiceClient;
import stackover.resource.service.repository.dto.CommentAnswerResponseDtoRepository;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CommentAnswerDtoServiceImpl implements CommentAnswerDtoService {

    private final CommentAnswerResponseDtoRepository dtoRepository;
    private final ProfileServiceClient profileServiceClient;

    @Override
    public Optional<CommentAnswerResponseDto> addCommentToAnswer(CommentAnswer commentAnswer, @Positive Long accountId) {
        CommentAnswerResponseDto commentAnswerResponseDto
                = dtoRepository.findByQuestionIdAndUserId(accountId, commentAnswer.getAnswer().getId()).orElse(null);

        if (commentAnswerResponseDto == null) {
            return Optional.empty();
        }


        String email = profileServiceClient.getUserEmail(accountId);

        return Optional.of(new CommentAnswerResponseDto(
                commentAnswerResponseDto.id(),
                commentAnswerResponseDto.answerId(),
                commentAnswerResponseDto.lastRedactionDate(),
                commentAnswerResponseDto.persistDate(),
                commentAnswerResponseDto.text(),
                commentAnswerResponseDto.userId(),
                email,
                commentAnswerResponseDto.imageLink(),
                commentAnswerResponseDto.reputation()
        ));

    }
}
