package stackover.resource.service.service.dto.impl;

import jakarta.persistence.EntityNotFoundException;
import org.springframework.stereotype.Service;
import stackover.resource.service.entity.user.User;
import stackover.resource.service.repository.entity.UserRepository;
import stackover.resource.service.service.dto.UserService;

import java.util.Optional;
@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Optional<User> findByAccountId(Long accountId) {
        return userRepository.findByAccountId(accountId)
                .or(() -> {
                    throw new EntityNotFoundException("User not found with accountId: " + accountId);
                });
    }
}
