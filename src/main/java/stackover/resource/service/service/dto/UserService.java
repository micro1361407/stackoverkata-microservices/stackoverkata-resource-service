package stackover.resource.service.service.dto;

import org.springframework.stereotype.Service;
import stackover.resource.service.entity.user.User;

import java.util.Optional;
//TODO переместить в пакет package stackover.resource.service.service.entity;
public interface UserService {
    Optional<User> findByAccountId(Long accountId);
}
