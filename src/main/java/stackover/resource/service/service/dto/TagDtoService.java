package stackover.resource.service.service.dto;

import stackover.resource.service.dto.responce.RelatedTagResponseDto;
import stackover.resource.service.dto.responce.TagResponseDto;

import java.util.List;

public interface TagDtoService {
    List<RelatedTagResponseDto> getTopTags();
    List<TagResponseDto> getTop3TagsByUserId(Long userId);
}
