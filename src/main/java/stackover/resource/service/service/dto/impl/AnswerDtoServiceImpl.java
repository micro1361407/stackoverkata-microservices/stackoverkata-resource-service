package stackover.resource.service.service.dto.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import stackover.resource.service.dto.request.AnswerRequestDto;
import stackover.resource.service.dto.responce.AnswerResponseDto;
import stackover.resource.service.repository.dto.AnswerDtoRepository;
import stackover.resource.service.service.dto.AnswerDtoService;
import stackover.resource.service.service.entity.AnswerService;
import java.util.List;
import java.util.Optional;

@Service
public class AnswerDtoServiceImpl implements AnswerDtoService {
    private final AnswerDtoRepository answerDtoRepository;
    private final AnswerService answerService;

    public AnswerDtoServiceImpl(AnswerDtoRepository answerDtoRepository, AnswerService answerService) {
        this.answerDtoRepository = answerDtoRepository;
        this.answerService = answerService;
    }

    @Override
    @Transactional(readOnly = true)
    public List<AnswerResponseDto> getAnswersDtoByQuestionId(Long questionId, Long accountId) {
        return answerDtoRepository.getAnswersDtoByQuestionId(questionId, accountId);
    }

    @Override
     public Optional<AnswerResponseDto> updateBodyAnswer(Long answerId, Long accountId, AnswerRequestDto answerDto) {
        answerService.updateBodyAnswer(answerId, answerDto);
        Optional<AnswerResponseDto> result = answerDtoRepository.getAnswersDtoByAnswerId(answerDto.id(), accountId);
        if (result.isPresent()) {
            return result;
        } else {
            throw new NullPointerException("AnswerResponseDto is null");
        }
    }
}