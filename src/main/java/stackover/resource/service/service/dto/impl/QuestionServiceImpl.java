package stackover.resource.service.service.dto.impl;

import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;
import stackover.resource.service.converters.QuestionMapper;
import stackover.resource.service.dto.request.QuestionCreateRequestDto;
import stackover.resource.service.entity.question.Question;
import stackover.resource.service.entity.question.Tag;
import stackover.resource.service.entity.user.User;
import stackover.resource.service.repository.entity.QuestionRepository;
import stackover.resource.service.service.entity.QuestionService;
import stackover.resource.service.service.entity.TagService;
import stackover.resource.service.service.dto.UserService;

import java.util.List;

@Service
public class QuestionServiceImpl implements QuestionService {

    private final QuestionRepository questionDtoRepository;
    private final QuestionMapper questionMapper;
    private final TagService tagService;
    private final UserService userService;

    public QuestionServiceImpl(QuestionRepository questionDtoRepository, QuestionMapper questionMapper, TagService tagService
            , UserService userService) {
        this.questionDtoRepository = questionDtoRepository;
        this.questionMapper = questionMapper;
        this.tagService = tagService;
        this.userService = userService;
    }

    @Override
    @Transactional
    public void addNewQuestion(QuestionCreateRequestDto request, Long accountId) {
        // TODO: временная реализация, реализовать проверку пользователя через сервис #stackover-auth-service
        User questionAuthor = userService.findByAccountId(accountId).orElseThrow(() -> new EntityNotFoundException("User not found"));

        Question questionToSave = questionMapper.questionDtoToQuestion(request);
        questionToSave.setUser(questionAuthor);

        List<Tag> savedTag = tagService.saveAll(request.tags());
        questionToSave.setTags(savedTag);

        questionDtoRepository.save(questionToSave);
    }
}