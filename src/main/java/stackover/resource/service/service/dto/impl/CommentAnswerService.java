package stackover.resource.service.service.dto.impl;


import stackover.resource.service.entity.question.answer.CommentAnswer;

public interface CommentAnswerService {
    CommentAnswer getCommentAnswer(Long answerId, String commentText, Long accountId);

}