package stackover.resource.service.service.dto.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import stackover.resource.service.dto.responce.RelatedTagResponseDto;

import stackover.resource.service.dto.responce.TagResponseDto;
import stackover.resource.service.repository.dto.TagDtoRepository;
import stackover.resource.service.service.dto.TagDtoService;

import java.util.List;

@Slf4j
@Service
public class TagDtoServiceImpl implements TagDtoService {

    private final TagDtoRepository tagDtoRepository;

    public TagDtoServiceImpl(TagDtoRepository tagDtoRepository) {
        this.tagDtoRepository = tagDtoRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public List<RelatedTagResponseDto> getTopTags() {
        PageRequest pageRequest = PageRequest.of(0, 10);
        return tagDtoRepository.getTopTags(pageRequest);
    }

    @Override
    @Transactional(readOnly = true)
    public List<TagResponseDto> getTop3TagsByUserId(Long userId) {
        PageRequest pageRequest = PageRequest.of(0, 3);
        List<TagResponseDto> result = tagDtoRepository.getTop3TagsByUserId(userId, pageRequest);
        log.info("Полученные теги для пользователя {}: {}", userId, result);
        return result;
    }
}
