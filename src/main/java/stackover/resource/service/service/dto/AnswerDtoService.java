package stackover.resource.service.service.dto;


import stackover.resource.service.dto.request.AnswerRequestDto;
import stackover.resource.service.dto.responce.AnswerResponseDto;

import java.util.List;
import java.util.Optional;

public interface AnswerDtoService {
    List<AnswerResponseDto> getAnswersDtoByQuestionId(Long questionId, Long accountId);
    Optional<AnswerResponseDto> updateBodyAnswer(Long answerId, Long accountId, AnswerRequestDto answerDto);
}
