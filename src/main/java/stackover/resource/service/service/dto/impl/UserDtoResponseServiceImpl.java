package stackover.resource.service.service.dto.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import stackover.resource.service.dto.responce.TagResponseDto;
import stackover.resource.service.dto.responce.UserResponseDto;
import stackover.resource.service.entity.question.Tag;
import stackover.resource.service.feign.ProfileServiceClient;
import stackover.resource.service.repository.dto.UserDtoRepository;
import stackover.resource.service.repository.entity.TagRepository;
import stackover.resource.service.service.dto.TagDtoService;
import stackover.resource.service.service.dto.UserDtoResponseService;

import java.util.List;
import java.util.Optional;


@Service
@Slf4j
@RequiredArgsConstructor
public class UserDtoResponseServiceImpl implements UserDtoResponseService {

    private final UserDtoRepository userDtoRepository;
    private final TagDtoService tagDtoService;

    private final ProfileServiceClient profileServiceClient;

    public Optional<UserResponseDto> getUserDtoByUserId(Long userId) {
        Optional<UserResponseDto> userDtoOpt = userDtoRepository.getUserDtoByUserId(userId);

        if (userDtoOpt.isEmpty()) {
            return Optional.empty();
        }

        UserResponseDto userDto = userDtoOpt.get();
        List<TagResponseDto> top3TagsByUserId = tagDtoService.getTop3TagsByUserId(userId);
        String email = profileServiceClient.getUserEmail(userId);

        return Optional.of(new UserResponseDto(
                userDto.id(),
                email,
                userDto.fullName(),
                userDto.linkImage(),
                userDto.city(),
                userDto.reputation(),
                userDto.registrationDate(),
                userDto.votes(),
                top3TagsByUserId
        ));
    }
}
