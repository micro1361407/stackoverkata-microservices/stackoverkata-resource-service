package stackover.resource.service.service.dto.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import stackover.resource.service.converters.TagMapper;
import stackover.resource.service.dto.request.QuestionCreateRequestDto;
import stackover.resource.service.dto.responce.QuestionResponseDto;
import stackover.resource.service.dto.responce.TagResponseDto;
import stackover.resource.service.repository.dto.QuestionDtoRepository;
import stackover.resource.service.service.dto.QuestionDtoResponseService;
import stackover.resource.service.service.entity.QuestionService;
import stackover.resource.service.entity.question.Tag;
import stackover.resource.service.service.entity.TagService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class QuestionDtoResponseServiceImpl implements QuestionDtoResponseService {

    private final QuestionService questionService;
    private final QuestionDtoRepository questionDtoRepository;
    private final TagService tagService;
    private final TagMapper tagMapper;

    @Override
    public QuestionResponseDto addNewQuestion(QuestionCreateRequestDto questionCreateRequestDto, Long accountId) {
        questionService.addNewQuestion(questionCreateRequestDto, accountId);
        return createResponseDto(questionCreateRequestDto, accountId);
    }

    @Override
    public Optional<QuestionResponseDto> getQuestionById(Long questionId, Long accountId) {
        Optional<QuestionResponseDto> dtoOpt = questionDtoRepository
                .getQuestionDtoByQuestionIdAndUserId(questionId, accountId);
        if (dtoOpt.isEmpty()) {
            return Optional.empty();
        }

        QuestionResponseDto dto = dtoOpt.get();
        List<TagResponseDto> tags = getTagsForQuestion(questionId);

        return Optional.of(rebuildDtoWithTags(dto, tags));
    }


    private QuestionResponseDto createResponseDto(QuestionCreateRequestDto questionCreateRequestDto, Long accountId) {
        return QuestionResponseDto.builder()
                .authorId(accountId)
                .title(questionCreateRequestDto.title())
                .description(questionCreateRequestDto.description())
                .tags(questionCreateRequestDto.tags())
                .build();
    }

    private List<TagResponseDto> getTagsForQuestion(Long questionId) {
        List<Tag> tagList = tagService.findTagsByQuestionId(questionId);
        List<TagResponseDto> tags = new ArrayList<>();

        for (Tag tag : tagList) {
            tags.add(tagMapper.entityToDto(tag));
        }
        return tags;
    }

    private QuestionResponseDto rebuildDtoWithTags(
            QuestionResponseDto dto, List<TagResponseDto> tags) {
        return new QuestionResponseDto(
                dto.id(),
                dto.title(),
                dto.authorId(),
                dto.authorName(),
                dto.authorImage(),
                dto.description(),
                dto.viewCount(),
                dto.authorReputation(),
                dto.countAnswer(),
                dto.countValuable(),
                dto.persistDateTime(),
                dto.lastUpdateDateTime(),
                dto.countVote(),
                dto.voteTypeQuestion(),
                tags
        );
    }
}


