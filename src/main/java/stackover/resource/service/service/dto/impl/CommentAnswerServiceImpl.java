package stackover.resource.service.service.dto.impl;

import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import stackover.resource.service.entity.CommentText;
import stackover.resource.service.entity.CommentType;
import stackover.resource.service.entity.question.answer.Answer;
import stackover.resource.service.entity.question.answer.CommentAnswer;
import stackover.resource.service.entity.user.User;
import stackover.resource.service.repository.entity.CommentAnswerRepository;
import stackover.resource.service.service.entity.AnswerService;
import stackover.resource.service.service.entity.UserService;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class CommentAnswerServiceImpl implements CommentAnswerService {

    private final CommentAnswerRepository commentAnswerRepository;
    private final AnswerService answerService;
    private final UserService userService;

    @Override
    public CommentAnswer getCommentAnswer(Long answerId, String commentText, Long accountId) {
        log.info("Fetching answer with id: {}", answerId);
        Answer answer = answerService.findById(answerId)
                .orElseThrow(() -> {
                    log.error("Answer not found with id: {}", answerId);
                    return new EntityNotFoundException("Answer not found with id: " + answerId);
                });

        log.info("Fetching user with id: {}", accountId);
        User user = userService.findById(accountId)
                .orElseThrow(() -> {
                    log.error("User  not found with id: {}", accountId);
                    return new EntityNotFoundException("User  not found with id: " + accountId);
                });

        CommentText commentTextEntity = new CommentText(CommentType.ANSWER);
        commentTextEntity.setText(commentText);
        commentTextEntity.setUser (user);

        CommentAnswer commentAnswer = new CommentAnswer();
        commentAnswer.setAnswer(answer);
        commentAnswer.setComment(commentTextEntity);

        try {
            CommentAnswer savedCommentAnswer = commentAnswerRepository.save(commentAnswer);
            log.info("CommentAnswer saved successfully with id: {}", savedCommentAnswer.getId());
            return savedCommentAnswer;
        } catch (Exception e) {
            log.error("Error saving CommentAnswer: {}", e.getMessage());
            throw new RuntimeException("Failed to save CommentAnswer");
        }
    }
}