package stackover.resource.service.service.dto;

import jakarta.validation.constraints.Positive;
import stackover.resource.service.dto.request.QuestionCreateRequestDto;
import stackover.resource.service.dto.responce.QuestionResponseDto;

import java.util.Optional;

public interface QuestionDtoResponseService {
    QuestionResponseDto addNewQuestion(QuestionCreateRequestDto questionCreateRequestDto, @Positive Long accountId);

    Optional<QuestionResponseDto> getQuestionById(Long questionId, Long accountId);
}