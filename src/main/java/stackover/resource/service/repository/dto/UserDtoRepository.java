package stackover.resource.service.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import stackover.resource.service.dto.responce.UserResponseDto;
import stackover.resource.service.entity.user.User;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

@Repository
public interface UserDtoRepository extends JpaRepository<User, Long> {
    @Query("""
       SELECT new stackover.resource.service.dto.responce.UserResponseDto(
           u.id,
           u.fullName,
           u.imageLink,
           u.city,
           COALESCE(SUM(r.count), 0),
           u.persistDateTime,
           COALESCE(SUM(CASE WHEN a.id IS NOT NULL THEN 1 ELSE 0 END), 0) +
           COALESCE(SUM(CASE WHEN q.id IS NOT NULL THEN 1 ELSE 0 END), 0) as votes
           
       )
       FROM User u
       LEFT JOIN Reputation r ON r.author.id = u.id
       LEFT JOIN VoteAnswer a ON a.user.id = u.id
       LEFT JOIN VoteQuestion q ON q.user.id = u.id
       WHERE u.id = :userId
       GROUP BY u.id, u.fullName, u.imageLink, u.city, u.persistDateTime
       """)
    Optional<UserResponseDto> getUserDtoByUserId(@Param("userId") Long userId);
}
