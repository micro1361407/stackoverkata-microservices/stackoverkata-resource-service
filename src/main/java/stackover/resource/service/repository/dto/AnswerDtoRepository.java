package stackover.resource.service.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import stackover.resource.service.dto.responce.AnswerResponseDto;
import stackover.resource.service.entity.question.answer.Answer;

import java.util.List;
import java.util.Optional;

@Repository
public interface AnswerDtoRepository extends JpaRepository<Answer, Long> {
    @Query("""
       SELECT new stackover.resource.service.dto.responce.AnswerResponseDto(
           a.id,
           a.user.id,
           a.question.id,
           a.htmlBody,
           a.persistDateTime,
           a.isHelpful,
           a.dateAcceptTime,
           SUM(CASE WHEN v.voteTypeAnswer = stackover.resource.service.entity.question.answer.VoteTypeAnswer.UP THEN 1 ELSE 0 END) AS countValuable,
           (SELECT SUM(r.count) from Reputation r WHERE r.author.id = :accountId) AS countUserReputation,
           a.user.imageLink,
           a.user.nickname,
           count (v) AS countVote,
           v.voteTypeAnswer AS voteTypeAnswer
       )
       FROM Answer a
       left join a.voteAnswers v
       where a.question.id = :questionId
       AND a.user.id = :accountId
       group by a.id, a.user.imageLink, a.user.nickname, a.user.id, a.isHelpful, a.persistDateTime, a.dateAcceptTime, v.voteTypeAnswer
       """)
    List<AnswerResponseDto> getAnswersDtoByQuestionId(Long questionId, Long accountId);


    @Query("""
    SELECT new stackover.resource.service.dto.responce.AnswerResponseDto(
        a.id,
        a.user.id,
        a.question.id,
        a.htmlBody,
        a.persistDateTime,
        a.isHelpful,
        a.dateAcceptTime,
        COUNT(CASE WHEN v.voteTypeAnswer = stackover.resource.service.entity.question.answer.VoteTypeAnswer.UP THEN 1 END) AS countValuable,
        COALESCE(SUM(r.count), 0) AS countUserReputation,
        a.user.imageLink,
        a.user.nickname,
        COUNT(v) AS countVote,
        v.voteTypeAnswer AS voteTypeAnswer
    )
    FROM Answer a
    LEFT JOIN a.voteAnswers v
    LEFT JOIN Reputation r ON r.author.id = :accountId
    WHERE a.id = :answerDtoId
    GROUP BY a.id, a.user.imageLink, a.user.nickname, a.user.id, a.isHelpful, a.persistDateTime, a.dateAcceptTime, v.voteTypeAnswer
""")
    Optional<AnswerResponseDto> getAnswersDtoByAnswerId(Long answerDtoId, Long accountId);
}