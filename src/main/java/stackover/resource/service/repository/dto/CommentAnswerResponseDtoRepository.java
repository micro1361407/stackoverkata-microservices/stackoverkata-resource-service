package stackover.resource.service.repository.dto;

import org.springframework.data.repository.query.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import stackover.resource.service.dto.responce.CommentAnswerResponseDto;
import stackover.resource.service.entity.question.answer.CommentAnswer;

import java.util.Optional;

public interface CommentAnswerResponseDtoRepository extends JpaRepository<CommentAnswer, Long> {

    @Query(value = """
            select new stackover.resource.service.dto.responce.CommentAnswerResponseDto(
                ca.id, ca.answer.id, a.persistDateTime, ca.commentText.text, a.user.id, a.user.imageLink, 
                (SELECT SUM(r.count) from Reputation r WHERE r.author.id = :accountId) AS countUserReputation) 
            from CommentAnswer ca 
            left join Answer a on ca.answer.id = a.id 
            where ca.answer.id = :answerId
            """)
    Optional<CommentAnswerResponseDto> findByQuestionIdAndUserId(
            @Param("accountId") Long accountId, @Param("answerId") Long answerId);
}