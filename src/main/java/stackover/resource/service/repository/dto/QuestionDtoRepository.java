package stackover.resource.service.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import stackover.resource.service.dto.responce.QuestionResponseDto;
import stackover.resource.service.entity.question.Question;
import org.springframework.data.repository.query.Param;
import java.util.Optional;

@Repository
public interface QuestionDtoRepository extends JpaRepository<Question, Long> {

    @Query("""
        SELECT new stackover.resource.service.dto.responce.QuestionResponseDto(
            q.id,
            q.title,
            q.user.id,
            q.user.fullName,
            q.user.imageLink,
            q.description,
            (SELECT COUNT(v) FROM QuestionViewed v WHERE v.question = q),
            (SELECT COALESCE(SUM(r.count), 0) FROM Reputation r WHERE r.author.id = q.user.id),
            (SELECT COUNT(a) FROM Answer a WHERE a.question = q),
            (SELECT COALESCE(SUM(CASE WHEN v.voteTypeQuestion = 'UP' THEN 1 ELSE -1 END), 0)
                FROM VoteQuestion v WHERE v.question = q),
            q.persistDateTime,
            q.lastUpdateDateTime,
            (SELECT COUNT(v) FROM VoteQuestion v WHERE v.question = q),
            (SELECT v.voteTypeQuestion FROM VoteQuestion v
                WHERE v.question = q AND v.user.id = :accountId)
        )
        FROM Question q
        WHERE q.id = :questionId
    """)
    Optional<QuestionResponseDto> getQuestionDtoByQuestionIdAndUserId(
            @Param("questionId") Long questionId,
            @Param("accountId") Long accountId);
}
