package stackover.resource.service.repository.dto;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import stackover.resource.service.dto.responce.RelatedTagResponseDto;

import stackover.resource.service.dto.responce.TagResponseDto;
import stackover.resource.service.entity.question.Tag;

import java.util.List;

@Repository
public interface TagDtoRepository extends JpaRepository<Tag, Long> {

    @Query("""
            SELECT new stackover.resource.service.dto.responce.RelatedTagResponseDto(t.id, t.name, COUNT(q.id))
            FROM Tag t
            INNER JOIN t.questions q
            GROUP BY t.id, t.name
            ORDER BY COUNT(q.id) DESC
            """)
    List<RelatedTagResponseDto> getTopTags(Pageable pageable);

    @Query("""
    SELECT new stackover.resource.service.dto.responce.TagResponseDto(
        t.id,
        t.name,
        t.description
    )
    FROM Tag t
    JOIN t.questions q
    JOIN Reputation r ON r.question.id = q.id AND r.type = stackover.resource.service.entity.user.reputation.ReputationType.VOTE_ANSWER
    WHERE q.user.accountId = :userId
    GROUP BY t.id, t.name, t.description
    ORDER BY COUNT(r.id) DESC
""")
    List<TagResponseDto> getTop3TagsByUserId(@Param("userId") Long userId, Pageable pageable);


}
