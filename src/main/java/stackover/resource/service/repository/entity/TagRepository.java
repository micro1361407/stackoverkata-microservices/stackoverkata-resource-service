package stackover.resource.service.repository.entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import stackover.resource.service.entity.question.Tag;

import java.util.Collection;
import java.util.List;

@Repository
public interface TagRepository extends JpaRepository<Tag, Long> {

    List<Tag> findAllByNameIn(Collection<String> names);

    @Query("""
        SELECT t
        FROM Question q
        JOIN q.tags t
        WHERE q.id = :questionId
        """)
    List<Tag> findTagsByQuestionId(@Param("questionId") Long questionId);
}