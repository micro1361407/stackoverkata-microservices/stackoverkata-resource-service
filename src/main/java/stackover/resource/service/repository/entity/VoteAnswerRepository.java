package stackover.resource.service.repository.entity;

import org.springframework.data.jpa.repository.JpaRepository;
import stackover.resource.service.entity.question.answer.VoteAnswer;

import java.util.Optional;


public interface VoteAnswerRepository extends JpaRepository<VoteAnswer, Long> {
    Long countVoteAnswersByAnswerId(Long answerId); //Общее количество голосов за ответ.
    Optional<VoteAnswer> findByAnswerIdAndUserId(Long answerId, Long userId);
}
