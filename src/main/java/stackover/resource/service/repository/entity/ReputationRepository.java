package stackover.resource.service.repository.entity;

import org.springframework.data.jpa.repository.JpaRepository;
import stackover.resource.service.entity.user.reputation.Reputation;

import java.util.Optional;

public interface ReputationRepository extends JpaRepository<Reputation, Long> {
    Optional<Reputation> findByAuthorIdAndSenderIdAndAnswerId(Long authorId, Long senderId, Long answerId);
}