package stackover.resource.service.repository.entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import stackover.resource.service.entity.question.Question;

@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {

}