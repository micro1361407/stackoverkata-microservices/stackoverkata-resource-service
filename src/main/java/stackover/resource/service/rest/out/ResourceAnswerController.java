package stackover.resource.service.rest.out;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.constraints.Positive;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import stackover.resource.service.dto.request.AnswerRequestDto;
import stackover.resource.service.dto.responce.AnswerResponseDto;
import stackover.resource.service.dto.responce.CommentAnswerResponseDto;
import stackover.resource.service.entity.question.answer.CommentAnswer;
import stackover.resource.service.feign.AuthServiceClient;
import stackover.resource.service.service.dto.AnswerDtoService;
import stackover.resource.service.service.dto.impl.CommentAnswerDtoService;
import stackover.resource.service.service.dto.impl.CommentAnswerService;
import stackover.resource.service.service.entity.AnswerService;
import stackover.resource.service.service.facade.VoteAnswerFacade;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/api/user/question/{questionId}/answer")
public class ResourceAnswerController {
    private final AnswerDtoService answerDtoService;
    private final VoteAnswerFacade voteAnswerFacade;
    private final AuthServiceClient authServiceClient;
    private final CommentAnswerService commentAnswerService;
    private final CommentAnswerDtoService commentAnswerDtoService;
    private final AnswerService answerService;

    public ResourceAnswerController(AnswerDtoService answerDtoService, AuthServiceClient authServiceClient, VoteAnswerFacade voteAnswerFacade, CommentAnswerService commentAnswerService, CommentAnswerDtoService commentAnswerDtoService, AnswerService answerService) {
        this.answerDtoService = answerDtoService;
        this.authServiceClient = authServiceClient;
        this.commentAnswerService = commentAnswerService;
        this.commentAnswerDtoService = commentAnswerDtoService;
        this.answerService = answerService;
        this.voteAnswerFacade = voteAnswerFacade;
    }

    @Operation(summary = "Получить все ответы на вопрос",
            description = "Возвращает список ответов на вопрос по его ID и ID аккаунта.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешно получены ответы"),
            @ApiResponse(responseCode = "404", description = "Вопрос не найден"),
            @ApiResponse(responseCode = "400", description = "Некорректный ID аккаунта")
    })
    @GetMapping
    public ResponseEntity<List<AnswerResponseDto>> getAllAnswers(@PathVariable Long questionId,
                                                                 @RequestParam @Positive Long accountId) {
        log.info("Запрос на получение ответов по questionId: {} и accountId: {}", questionId, accountId);
        if (!authServiceClient.isAccountExist(accountId)) {
            log.error("Аккаунт с id: {} не существует", accountId);
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body(Collections.singletonList(new AnswerResponseDto(
                            null, accountId, questionId, "Unknown",
                            null, false, null, 0L, 0L,
                            "Unknown", "Unknown", 0L, null)));
        }
        List<AnswerResponseDto> answers = answerDtoService.getAnswersDtoByQuestionId(questionId, accountId);
        log.info("Найдено ответов: {}", answers.size());

        return ResponseEntity.ok(answers);
    }

    @Operation(summary = "Добавить комментарий к ответу",
            description = "Добавляет комментарий к ответу по ID вопроса и ID ответа.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Комментарий успешно добавлен"),
            @ApiResponse(responseCode = "404", description = "Ответ или аккаунт не найден"),
            @ApiResponse(responseCode = "400", description = "Некорректные данные запроса")
    })
    @PostMapping("/{answerId}/comment")
    public ResponseEntity<CommentAnswerResponseDto> addCommentToAnswer(
            @PathVariable Long questionId,
            @PathVariable Long answerId,
            @RequestParam @Positive Long accountId,
            @RequestBody String commentText) {
        log.info("Запрос на добавление комментария к ответу с answerId: {} для questionId: {} и accountId: {}", answerId, questionId, accountId);


        if (!authServiceClient.isAccountExist(accountId)) {
            log.error("Аккаунт с id: {} не существует", accountId);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        CommentAnswer commentAnswer = commentAnswerService.getCommentAnswer(answerId, commentText, accountId);
        return commentAnswerDtoService.addCommentToAnswer(commentAnswer, accountId)
                .map(commentAnswerDto -> {
                    log.info("Комментарий успешно добавлен к ответу с answerId: {}", answerId);
                    return ResponseEntity.status(HttpStatus.CREATED).body(commentAnswerDto);
                })
                .orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CommentAnswerResponseDto(accountId)));
    }

    @Operation(summary = "Уменьшить оценку ответа",
            description = "Уменьшает оценку ответа и обновляет репутацию автора ответа.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешно изменена оценка ответа"),
            @ApiResponse(responseCode = "404", description = "Аккаунт не найден"),
            @ApiResponse(responseCode = "400", description = "Некорректный запрос"),
            @ApiResponse(responseCode = "500", description = "Внутренняя ошибка сервера")
    })
    @PostMapping("/{answerId}/vote")
    public ResponseEntity<Long> voteAnswer(@PathVariable Long questionId, @PathVariable Long answerId,
                                            @RequestParam @Positive Long accountId, @RequestParam String voteType) {
        log.info("Запрос на голосование за ответ - изменить оценку по questionId: {}, answerId: {}, accountId: {}, voteType: {}", questionId, answerId, accountId, voteType);
        if (!authServiceClient.isAccountExist(accountId)) {
            log.error("Аккаунт с id: {} не существует", accountId);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        Long totalVotes = voteAnswerFacade.createNewVoteAnswer(accountId, answerId, voteType);
        log.info("Изменена оценка ответа. Total votes:{}", totalVotes);
        return ResponseEntity.ok(totalVotes);   
    }

    @Operation(summary = "Обновить тело ответа",
            description = "Возвращает ответ на вопрос по его ID и ID вопроса")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешно получен ответ"),
            @ApiResponse(responseCode = "404", description = "Вопрос не найден"),
            @ApiResponse(responseCode = "400", description = "Некорректный ID аккаунта"),
            @ApiResponse(responseCode = "500", description = "Внутренняя ошибка сервера")
    })
    @PutMapping("/{answerId}/body")
    public ResponseEntity<AnswerResponseDto> updateAnswerBody(@RequestBody AnswerRequestDto answerRequestDto,
                                                              @RequestParam @Positive Long accountId,
                                                              @PathVariable Long questionId,
                                                              @PathVariable Long answerId) {
        log.info("Проверка на наличие аккаунта по id: {}", accountId);
        if (!authServiceClient.isAccountExist(accountId)) {
            log.error("Аккаунт с id {} не существует ", accountId);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(new AnswerResponseDto(
                            null, accountId, questionId, "Unknown",
                            null, false, null, 0L, 0L,
                            "Unknown", "Unknown", 0L, null));
        }
        log.info("Обновление тела ответа и получение ответа answerResponseDto из answerRequestDto: {}", answerRequestDto);
        Optional<AnswerResponseDto> result = answerDtoService.updateBodyAnswer(answerId, accountId, answerRequestDto);
        if (result.isPresent()) {
            log.info("Успешное изменение тела ответа");
            return ResponseEntity.ok(result.get());
        } else {
            return ResponseEntity.internalServerError().body(null);
        }
    }
}


