package stackover.resource.service.rest.out;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.persistence.EntityNotFoundException;
import jakarta.validation.constraints.Positive;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import stackover.resource.service.dto.responce.RelatedTagResponseDto;
import stackover.resource.service.dto.responce.TagResponseDto;
import stackover.resource.service.service.dto.TagDtoService;
import stackover.resource.service.service.dto.UserService;

import java.util.List;

@RestController
@Slf4j
@RequestMapping("/api/user/tag")
public class ResourceTagController {

    private final TagDtoService tagDtoService;
    private final UserService userService;

    public ResourceTagController(TagDtoService tagDtoService, UserService userService) {
        this.tagDtoService = tagDtoService;
        this.userService = userService;
    }

    @Operation(summary = "Получить топ 10 тегов RelatedTagResponseDto",
            description = "Возвращает список RelatedTagResponseDto, который состоит из 10 топ тегов.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешно получен список"),
            @ApiResponse(responseCode = "500", description = "Список null")
    })
    @GetMapping("/related")
    public ResponseEntity<List<RelatedTagResponseDto>> getTop10Tags() {
        log.info("Запрос на получение списка топ 10 тегов RelatedTagResponseDto");
        List<RelatedTagResponseDto> result = tagDtoService.getTopTags();
        log.info("Успешное получение списка");
        return ResponseEntity.ok(result);

    }

    @Operation(summary = "Получить топ-3 тега пользователя",
            description = "Возвращает список из трех топ тегов, в которых пользователь заработал больше всего репутации.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешно получен список"),
            @ApiResponse(responseCode = "404", description = "Пользователь не найден"),
            @ApiResponse(responseCode = "400", description = "Некорректный ID аккаунта"),
            @ApiResponse(responseCode = "500", description = "Ошибка сервера")
    })
    @GetMapping("/top-3tags")
    public ResponseEntity<List<TagResponseDto>> getTop3TagsUserId(@RequestParam @Positive Long accountId) {
        log.info("Запрос на получение топ-3 тегов для пользователя с ID: {}", accountId);

        if (accountId == null || accountId <= 0) {
            log.error("Некорректный ID аккаунта");
            return ResponseEntity.badRequest().build();
        }

        try {
            userService.findByAccountId(accountId).orElseThrow(() ->
                    new EntityNotFoundException("Пользователь не найден с accountId: " + accountId));
            List<TagResponseDto> result = tagDtoService.getTop3TagsByUserId(accountId);
            return ResponseEntity.ok(result);
        } catch (EntityNotFoundException e) {
            log.error("Пользователь с ID {} не найден", accountId);
            return ResponseEntity.ok(List.of());
        }
    }
}
