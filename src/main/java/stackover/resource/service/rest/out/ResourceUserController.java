package stackover.resource.service.rest.out;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import stackover.resource.service.dto.responce.UserResponseDto;
import stackover.resource.service.service.dto.UserDtoResponseService;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/api/user")
@RequiredArgsConstructor
@Slf4j
@Tag(name = "User Controller", description = "Контроллер для работы с пользователями")
public class ResourceUserController {

    private final UserDtoResponseService userDtoResponseService;

    @Operation(summary = "Получить пользователя по ID", description = "Возвращает информацию о пользователе по его ID")
    @GetMapping("/{userId}")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "DTO пользователя получен"),
            @ApiResponse(responseCode = "404", description = "Пользователь не найден")
    })

    public ResponseEntity<UserResponseDto> getUserDtoByUserId(@PathVariable("userId") Long userId) {
        return userDtoResponseService.getUserDtoByUserId(userId)
                .map(userResponseDto -> {
                    log.info("Result endpoint: {}", userResponseDto);
                    return ResponseEntity.ok(userResponseDto);
                })
                .orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND)
                        .body(new UserResponseDto(
                                userId,
                                "Unknown",
                                "Unknown",
                                "Unknown",
                                "Unknown",
                                0L,
                                LocalDateTime.now(),
                                0L,
                                List.of()
                        )));
    }

}
