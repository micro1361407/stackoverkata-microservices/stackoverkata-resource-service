package stackover.resource.service.rest.out;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.constraints.Positive;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import stackover.resource.service.dto.request.QuestionCreateRequestDto;
import stackover.resource.service.dto.responce.QuestionResponseDto;
import stackover.resource.service.feign.AuthServiceClient;
import stackover.resource.service.service.dto.QuestionDtoResponseService;

import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("api/user/question")
public class ResourceQuestionController {


    private final QuestionDtoResponseService questionDtoResponseService;
    private final AuthServiceClient authServiceClient;

    public ResourceQuestionController(QuestionDtoResponseService questionDtoResponseService,
                                      AuthServiceClient authServiceClient) {
        this.questionDtoResponseService = questionDtoResponseService;
        this.authServiceClient = authServiceClient;
    }

    @Operation(summary = "Добавить новый вопрос",
            description = "Добавляет новый вопрос в БД. Принимает в качестве параметров ID аккаунта (accountId) и объект QuestionCreateRequestDto, содержащий информацию о вопросе. Возвращает объект QuestionResponseDto с информацией о добавленном вопросе.",
            parameters = {
                    @Parameter(name = "questionCreateRequestDto",
                            description = "Содержит информацию о вопросе"
                    ),
                    @Parameter(name = "accountId",
                            description = "ID аккаунта"
                    )}

    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Вопрос добавлен успешно"),
            @ApiResponse(responseCode = "403", description = "Пользователь не имеет прав на добавление вопроса"),
            @ApiResponse(responseCode = "400", description = "Ошибка в запросе"),
            @ApiResponse(responseCode = "500", description = "Ошибка на сервере при обработке запроса"),
    })
    @PostMapping
    public ResponseEntity<QuestionResponseDto> addNewQuestion(@RequestParam @Positive Long accountId,
                                                              @RequestBody QuestionCreateRequestDto questionCreateRequestDto) {

        log.info("Start processing addNewQuestion with request parameters questionCreateRequestDto: {}, accountId: {} ", questionCreateRequestDto, accountId);
        QuestionResponseDto response = questionDtoResponseService.addNewQuestion(questionCreateRequestDto, accountId);
        log.info("successfully processed addNewQuestion with response: {}", response);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Operation(summary = "Получить вопрос по ID",
            description = "Возвращает DTO вопроса по его ID и ID аккаунта")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "DTO вопроса получен"),
            @ApiResponse(responseCode = "404", description = "Вопрос не найден"),
            @ApiResponse(responseCode = "400", description = "Некорректный ID аккаунта")
    })
    @GetMapping("/{questionId}")
    public ResponseEntity<QuestionResponseDto> getQuestionById(
            @PathVariable Long questionId,
            @RequestParam @Positive Long accountId) {

        log.info("Запрос на получение ответов по questionId: {} и accountId: {}", questionId, accountId);
        if (!authServiceClient.isAccountExist(accountId)) {
            log.warn("Аккаунт с id: {} не найден", accountId);
            return ResponseEntity.badRequest().build();
        }

        Optional<QuestionResponseDto> questionResponseDto = questionDtoResponseService.getQuestionById(questionId, accountId);

        if (questionResponseDto.isPresent()) {
            log.info("Вопрос с ID: {} для аккаунта с ID: {} найден", questionId, accountId);
            return ResponseEntity.ok(questionResponseDto.get());
        }
        log.warn("Вопрос с ID: {} для аккаунта с ID: {} не найден", questionId, accountId);
        return ResponseEntity.notFound().build();
    }
}




