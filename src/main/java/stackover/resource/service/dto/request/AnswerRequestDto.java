package stackover.resource.service.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

import java.time.LocalDateTime;


public record AnswerRequestDto(
        @Schema(description = "id ответа на вопрос")
        Long id,
        @Schema(description = "id пользователя")
        Long userId,
        @Schema(description = "id вопроса")
        Long questionId,
        @NotEmpty
        @NotBlank
        @NotNull
        @Schema(description = "Текст ответа")
        String body,
        @Schema(description = "Дата создания ответа")
        LocalDateTime persistDate,
        @Schema(description = "Дата решения вопроса")
        LocalDateTime dateAccept){}
