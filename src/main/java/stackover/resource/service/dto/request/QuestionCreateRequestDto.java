package stackover.resource.service.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import stackover.resource.service.dto.responce.TagResponseDto;

import java.util.List;

public record QuestionCreateRequestDto(
        @Schema(description = "Заголовок создаваемого вопроса")
        @NotNull
        String title,
        @Schema(description = "Описание создаваемого вопроса")
        @NotNull
        String description,
        @Schema(description = "Теги создаваемого вопроса")
        @NotNull
        List<TagResponseDto> tags) {}