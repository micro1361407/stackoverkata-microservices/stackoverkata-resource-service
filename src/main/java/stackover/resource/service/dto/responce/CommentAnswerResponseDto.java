package stackover.resource.service.dto.responce;

import io.swagger.v3.oas.annotations.media.Schema;

import java.time.LocalDateTime;

@Schema(description = "DTO для ответа с данными комментария к ответу")
public record CommentAnswerResponseDto(
        @Schema(description = "ID комментария", example = "1")
        Long id,

        @Schema(description = "ID ответа", example = "123")
        Long answerId,

        @Schema(description = "Дата последнего редактирования", example = "2023-10-01T12:00:00")
        LocalDateTime lastRedactionDate,

        @Schema(description = "Дата создания комментария", example = "2023-10-01T12:00:00")
        LocalDateTime persistDate,

        @Schema(description = "Текст комментария", example = "Это пример комментария")
        String text,

        @Schema(description = "ID пользователя", example = "456")
        Long userId,

        @Schema(description = "Почта пользователя", example = "user@example.com")
        String email,

        @Schema(description = "Ссылка на картинку пользователя", example = "https://example.com/image.png")
        String imageLink,

        @Schema(description = "Репутация пользователя", example = "1000")
        Long reputation
) {
        public CommentAnswerResponseDto(Long id, Long answerId, LocalDateTime persistDateTime, String text,
                                        Long userId, String imageLink, Long reputation) {
                this(id, answerId, LocalDateTime.now(), persistDateTime, text, userId, null, imageLink, reputation);
        }

        public CommentAnswerResponseDto(Long accountId) {
                this(0L, null, LocalDateTime.now(), null, null, accountId, null, null, 0L);
        }
}