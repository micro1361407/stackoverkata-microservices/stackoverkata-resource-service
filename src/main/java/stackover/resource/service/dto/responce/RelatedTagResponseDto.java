package stackover.resource.service.dto.responce;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

public record RelatedTagResponseDto(
        @Schema(description = "id тега")
        Long id,
        @Schema(description = "Название тега")
        @NotEmpty
        @NotBlank
        @NotNull
        String title,
        @Schema(description = "Количество вопросов, помеченных данным тегом")
        Long countQuestion
)
{}
