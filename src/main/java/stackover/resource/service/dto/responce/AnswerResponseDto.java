package stackover.resource.service.dto.responce;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import stackover.resource.service.entity.question.answer.VoteTypeAnswer;

import java.time.LocalDateTime;

public record AnswerResponseDto (
        @Schema(description = "id ответа на вопрос")
        Long id,
        @Schema(description = "id пользователя")
        Long userId,
        @Schema(description = "id вопроса")
        Long questionId,
        @NotEmpty
        @NotBlank
        @NotNull
        @Schema(description = "Текст ответа")
        String body,
        @Schema(description = "Дата создания ответа")
        LocalDateTime persistDate,
        @Schema(description = "Польза ответа")
        Boolean isHelpful,
        @Schema(description = "Дата решения вопроса")
        LocalDateTime dateAccept,
        @Schema(description = "Рейтинг ответа")
        Long countValuable,
        @Schema(description = "Рейтинг юзера")
        Long countUserReputation,
        @Schema(description = "Ссылка на картинку пользователя")
        String image,
        @Schema(description = "Никнейм пользователя")
        String nickname,
        @Schema(description = "Количество голосов")
        Long countVote,
        @Schema(description = "тип голоса")
        VoteTypeAnswer voteTypeAnswer){}