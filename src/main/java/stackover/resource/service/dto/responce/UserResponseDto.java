package stackover.resource.service.dto.responce;

import io.swagger.v3.oas.annotations.media.Schema;

import java.time.LocalDateTime;
import java.util.List;

@Schema(description = "DTO для ответа с информацией о пользователе")
public record UserResponseDto(

        @Schema(description = "ID пользователя", example = "1") Long id,
        @Schema(description = "Почта пользователя", example = "user@example.com") String email,
        @Schema(description = "Имя пользователя", example = "Иван Иванов") String fullName,
        @Schema(description = "Ссылка на изображение пользователя", example = "http://example.com/image.jpg") String linkImage,
        @Schema(description = "Город пользователя", example = "Москва") String city,
        @Schema(description = "Репутация пользователя", example = "100") Long reputation,
        @Schema(description = "Дата регистрации пользователя", example = "2023-01-01T12:00:00") LocalDateTime registrationDate,
        @Schema(description = "Количество голосов пользователя", example = "50") Long votes,
        @Schema(description = "Список топ-3 тэгов пользователя") List<TagResponseDto> listTop3TagDto
) {
    public UserResponseDto(Long id, String fullName, String imageLink, String city,
                           Long reputation, LocalDateTime persistDateTime, Long votes) {
        this(id, null, fullName, imageLink, city, reputation, persistDateTime, votes, null);
    }
}
