package stackover.resource.service.dto.responce;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "DTO для ответа с информацией о тэге")
public record TagResponseDto(
        @Schema(description = "ID тэга", example = "1") Long id,
        @Schema(description = "Название тэга", example = "Java") String name,
        @Schema(description = "Описание тэга", example = "Вопросы по языку программирования Java") String description
) {}

