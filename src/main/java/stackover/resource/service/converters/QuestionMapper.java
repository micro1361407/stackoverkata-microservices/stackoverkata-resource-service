package stackover.resource.service.converters;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import stackover.resource.service.dto.request.QuestionCreateRequestDto;
import stackover.resource.service.dto.responce.QuestionResponseDto;
import stackover.resource.service.entity.question.Question;


@Mapper(componentModel = "spring")
public interface QuestionMapper {

    @Mapping(target = "id", source = "id")
    @Mapping(target = "title", source = "title")
    @Mapping(target = "description", source = "description")
    QuestionResponseDto entityToDto(Question question);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "tags", ignore = true)
    Question questionDtoToQuestion(QuestionCreateRequestDto source);
}