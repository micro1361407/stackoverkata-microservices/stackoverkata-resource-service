package stackover.resource.service.converters;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import stackover.resource.service.dto.responce.TagResponseDto;
import stackover.resource.service.entity.question.Tag;

@Mapper(componentModel = "spring")
public interface TagMapper {

    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    @Mapping(target = "description", source = "description")
    TagResponseDto entityToDto(Tag source);

    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    @Mapping(target = "description", source = "description")
    Tag DtoToEntity(TagResponseDto source);
}
