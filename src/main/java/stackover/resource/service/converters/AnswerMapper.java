package stackover.resource.service.converters;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import stackover.resource.service.dto.request.AnswerRequestDto;
import stackover.resource.service.dto.responce.AnswerResponseDto;
import stackover.resource.service.entity.question.answer.Answer;


@Mapper(componentModel = "spring")
public interface AnswerMapper {

    @Mapping(target = "id", source = "id")
    @Mapping(target = "userId", source = "user.id")
    @Mapping(target = "questionId", source = "question.id")
    @Mapping(target = "body", source = "htmlBody")
    @Mapping(target = "persistDate", source = "persistDateTime")
    @Mapping(target = "dateAccept", source = "dateAcceptTime")
    AnswerResponseDto entityToDto(Answer answer);


    @Mapping(target = "id", source = "id")
    @Mapping(target = "user.id", source = "userId")
    @Mapping(target = "question.id", source = "questionId")
    @Mapping(target ="htmlBody", source = "body")
    @Mapping(target = "persistDateTime", source = "persistDate")
    @Mapping(target = "dateAcceptTime", source = "dateAccept")
    Answer dtoToEntity(AnswerRequestDto answer);
}
