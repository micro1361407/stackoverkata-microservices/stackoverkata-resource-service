package stackover.resource.service.feign;

import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.stereotype.Component;
import stackover.resource.service.exception.ProfileNotFoundException;

@FeignClient(value = "stackoverkata-profile-service", fallbackFactory = ProfileServiceClient.ProfileServiceFallbackFactory.class)
public interface ProfileServiceClient {

    @GetMapping("/api/internal/profile/{accountId}/email")
    String getUserEmail(@PathVariable Long accountId); //TODO: реализовать на стороне profile-service

    @Component
    class ProfileServiceFallbackFactory implements FallbackFactory<FallbackWithFactory> {
        @Override
        public FallbackWithFactory create(Throwable cause) {
            return new FallbackWithFactory(cause.getMessage());
        }
    }

    record FallbackWithFactory(String reason) implements ProfileServiceClient {
        @Override
        public String getUserEmail(@PathVariable Long accountId) {
            throw new ProfileNotFoundException("Email for account ID %s not found".formatted(accountId));
        }
    }
}
